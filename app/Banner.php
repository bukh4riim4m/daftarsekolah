<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
  protected $fillable = [
      'title','logo', 'telpon','email','alamat','admin_id'
  ];
}
