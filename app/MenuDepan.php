<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuDepan extends Model
{
    protected $fillable = [
        'menu','url', 'is_active'
    ];
}
