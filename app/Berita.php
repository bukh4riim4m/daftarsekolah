<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $fillable = [
        'judul', 'isi', 'tgl_posting','gambar','like','petugas_id','is_active'
    ];
}
