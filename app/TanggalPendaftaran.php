<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TanggalPendaftaran extends Model
{
    protected $fillable = [
        'mulai','selesai'
    ];
}
