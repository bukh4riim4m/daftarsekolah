<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserKomen extends Authenticatable
{
    use Notifiable;
    protected $table = "user_komens";
    protected $primaryKey = "id";

    protected $fillable = [
        'name', 'email','provider','provider_id','rememberToken', 'password','type'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

}
