<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = "users";
    protected $primaryKey = "id";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nomor_pendaftaran','name', 'email','hp','foto', 'password','hp','nama_panggilan','jenis_kelamin_id','tempat_lahir','tgl_lahir','alamat_lengkap','sekolah_asal','alamat_sekolah_asal','jumlah_saudara_kandung','jumlah_saudara_tiri','jumlah_saudara_angkat','yatim_piyatu_id','bahasa_sehari_hari','bangsa_negara','kelainan_jasmani','penyakit','tinggi_badan','berat_badan','prestasi','nama_ayah','tempat_lahir_ayah','tgl_lahir_ayah','agama_ayah_id','pendiddikan_ayah_id','pekerjaan_ayah','warga_negara_ayah','nomor_telpon_ayah','pedapatan_ayah','nama_ibu','tempat_lahir_ibu','tgl_lahir_ibu','agama_ibu_id','pendiddikan_ibu_id','pekerjaan_ibu','warga_negara_ibu','nomor_telpon_ibu','pedapatan_ibu','status_id','petugas_id','aktif'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function jenisKelaminSiswa(){
        return $this->belongsTo('App\JenisKelamin','jenis_kelamin_id');
    }
    public function anakYatimPiyatu(){
        return $this->belongsTo('App\YatimPiyatu','yatim_piyatu_id');
    }
    public function agamaAyah(){
        return $this->belongsTo('App\Agama','agama_ayah_id');
    }
    public function agamaIbu(){
        return $this->belongsTo('App\Agama','agama_ibu_id');
    }
    public function pendidikanAyah(){
        return $this->belongsTo('App\Pendidikan','pendiddikan_ayah_id');
    }
    public function pendidikanIbu(){
        return $this->belongsTo('App\Pendidikan','pendiddikan_ibu_id');
    }
    public function pendapatanAyah(){
        return $this->belongsTo('App\Pendapatan','pedapatan_ayah');
    }
    public function pendapatanIbu(){
        return $this->belongsTo('App\Pendapatan','pedapatan_ibu');
    }
}
