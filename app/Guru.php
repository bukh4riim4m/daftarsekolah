<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Guru extends Authenticatable
{
    use Notifiable;
    protected $table = "gurus";
    protected $primaryKey = "id";

    protected $fillable = [
        'name', 'email','hp', 'password','link_fb','kata_mutiara','foto','kategori_id','petugas_id'
    ];
    public function KategoriGuru(){
        return $this->belongsTo('App\KategoriGuru','kategori_id');
    }

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
