<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AksesUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && Auth::guard('user')->check() && $request->user()->type=='user' && $request->user()->aktif=='yes'){
            return $next($request);
        }
        $request->session()->invalidate();
        return redirect()->guest('/masuk');
    }
}
