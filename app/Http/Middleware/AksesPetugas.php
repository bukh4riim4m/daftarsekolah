<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AksesPetugas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->type=='petugas' && Auth::guard('petugas')->check()){
            return $next($request);
        }
        return redirect()->guest('/masuk');
    }
}
