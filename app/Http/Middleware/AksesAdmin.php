<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class AksesAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && Auth::guard('admin')->check() && $request->user()->type=='admin'){
            return $next($request);
        }
        return redirect()->guest('/masuk');
    }
}
