<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AksesKomen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->type=='komen' && $request->user()->provider=='facebook'){
            return $next($request);
        }
        return redirect()->back();
    }
}
