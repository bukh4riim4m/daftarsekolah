<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AksesGuru
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->type=='guru' && $request->user()->is_active =='yes' && Auth::guard('guru')->check()){
            return $next($request);
        }
        return redirect()->guest('/masuk');
    }
}
