<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Petugas;
use App\Guru;
use App\MenuDepan;
use App\TanggalPendaftaran;
use DB;
use Log;
use Image;

class AdminController extends Controller
{
  public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(){
      return view('admin.index');
    }
    public function datacalonsiswa(Request $request){
      $siswas = User::get();
      return view('admin.data_calon_siswa',compact('siswas'));
    }
    public function datasiswaditerima(Request $request){
      $siswas = User::get();
      return view('admin.data_calon_siswa',compact('siswas'));
    }
    public function dataKaryawan(Request $request){
      if ($request->action =='add') {
          $message = [
              'nama.required'=>'Nama wajib di isi',
              'nama.min'=>'Nama minimal 3 Karakter',
              'foto.required'=>'Foto wajib di masukan',
              'foto.image'=>'Foto Harus Format JPG/PNG',
              'foto.max'=>'Maksimal 2 MB',
              'email.required'=>'Email Wajib diisi',
              'email.min'=>'Email minimal 3 Karakter',
              'email.email'=>'Email tidak valid',
              'email.unique'=>'Email Sudah digunakan',
              'hp.required'=>'Nomo Hp Wajib diisi',
              'hp.min'=>'Email minimal 10 Karakter',
              'hp.unique'=>'Nomor Sudah digunakan',
              'password.required'=>'Password Wajib diisi',
              'password.min'=>'Minimal Password 6 karakter',
          ];
          $this->validate($request, [
              'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
              'nama'=>'required|min:3',
              'email'=>'required|min:3|email|unique:users|unique:admins|unique:petugas|unique:gurus|min:10',
              'hp'=>'required|unique:users|unique:admins|unique:petugas|unique:gurus|min:10',
              'password'=>'required|min:6',
          ],$message);
          DB::beginTransaction();
          try {
              $image = $request->file('foto');
              $imageName = $image->getClientOriginalName();
              $fileName = date('YmdHis')."_".$imageName;
              $directory = public_path('/images/karyawan/');
              $imageUrl = $directory.$fileName;
              Image::make($image)->resize(262, 300)->save($imageUrl);
              Petugas::create([
                  'name'=>$request->nama,
                  'email'=>$request->email,
                  'hp'=>$request->hp,
                  'password'=>Hash::make($request->password),
                  'type'=>'petugas',
                  'link_fb'=>$request->link_fb,
                  'foto'=>$fileName,
                  'link_fb'=>$request->link_fb,
                  'instagram'=>$request->instagram,
                  'open'=>'yes',
                  'is_active'=>'yes',
                  'admin_id'=>$request->user()->id
              ]);
          } catch (\Throwable $th) {
              Log::info('Gagal Edit Profil:'.$th->getMessage());
              DB::rollback();
              flash('Maaf! Gagal disimpan.')->error();
              return redirect()->back();
          }
          DB::commit();
          flash('Berhasil disimpan')->important();
          return redirect()->back();
      }
      $karyawan =  Petugas::where('is_active','yes')->get();
      return view('admin.karyawan',compact('karyawan'));
    }
    public function dataGuru(Request $request){
      $karyawan =  Guru::where('is_active','yes')->get();
      return view('admin.guru',compact('karyawan'));
    }
    public function pengaturan(Request $request){
      if ($request->action =='menu') {
        $ids = $request->ids;
        $active = $request->is_active;
        DB::beginTransaction();
        try {
          foreach ($ids as $key => $id) {
            MenuDepan::find($id)->update([
              'is_active'=>$active[$key]
            ]);
          }
        } catch (\Throwable $th) {
          Log::info('Gagal Edit Menu:'.$th->getMessage());
          DB::rollback();
          flash('Menu Gagal diubah.')->error();
          return redirect()->back();
        }
        DB::commit();
        flash('Menu Berhasil diubah')->important();
        return redirect()->back();
      }elseif($request->action =='tgl') {
        $tgl = TanggalPendaftaran::find(1)->update([
          'mulai'=>date('Y-m-d', strtotime($request->mulai)),
          'selesai'=>date('Y-m-d', strtotime($request->selesai))
        ]);
        if ($tgl) {
          flash('Tanggal Berhasil update')->important();
          return redirect()->back();
        } 
        flash('Tanggal Gagal diubah.')->error();
        return redirect()->back();
      }
      $tanggal = TanggalPendaftaran::find(1);
      return view('admin.pengaturan',compact('tanggal'));
    }

    public function adminberita(){
      return view('admin.berita');
    }
}
