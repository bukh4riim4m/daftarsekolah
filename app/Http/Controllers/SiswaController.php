<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use App\User;
use App\AlurPendaftaran;
use Auth;
use Image;
use PDF;
use View;
use Log;
use DB;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('user');
    }
    public function index(){
        return view('siswa.index');
    }
    public function datasaya(){
        return view('siswa.data_saya');
    }
    public function download(Request $request){
        $users = User::find($request->user()->id);
        $pdf = PDF::loadView('export.bukti_pendaftaran',compact('users'));
	    return $pdf->download('bukti_pendaftaran.pdf');
    }
    public function edit(Request $request){
        $datas = $request->user();
        return view('siswa.edit_data_saya',compact('datas'));
    }
    public function update(Request $request){
        $message = [
            'name.required'=>'Tidak Boleh Kosong', 
            'foto.required'=>'Foto Wajib Masukan',
            'foto.image'=>'Format gambar',
            
            'hp.required'=>'Tidak Boleh Kosong',
            'hp.unique'=>'Nomor Telp Sudah digunakan',
            'hp.min'=>'Nomor Telp Minimal 10 digit',
            'nama_panggilan.required'=>'Tidak Boleh Kosong',
            'jenis_kelamin_id.required'=>'Tidak Boleh Kosong',
            'tempat_lahir.required'=>'Tidak Boleh Kosong',
            'tgl_lahir.required'=>'Harus Pilih',
            'bulan_lahir.required'=>'Harus Pilih',
            'tahun_lahir.required'=>'Harus Pilih',
            'alamat_lengkap.required'=>'Tidak Boleh Kosong',
            'sekolah_asal.required'=>'Tidak Boleh Kosong',
            'alamat_sekolah_asal.required'=>'Tidak Boleh Kosong',
            'jumlah_saudara_kandung.required'=>'Tidak Boleh Kosong',
            'jumlah_saudara_tiri.required'=>'Tidak Boleh Kosong',
            'jumlah_saudara_angkat.required'=>'Tidak Boleh Kosong',
            'yatim_piyatu_id.required'=>'Tidak Boleh Kosong',
            'bahasa_sehari_hari.required'=>'Tidak Boleh Kosong',
            'bangsa_negara.required'=>'Tidak Boleh Kosong',
            'kelainan_jasmani.required'=>'Tidak Boleh Kosong',
            'penyakit.required'=>'Tidak Boleh Kosong',
            'tinggi_badan.required'=>'Tidak Boleh Kosong',
            'berat_badan.required'=>'Tidak Boleh Kosong',
            'prestasi.required'=>'Tidak Boleh Kosong',
            'nama_ayah.required'=>'Tidak Boleh Kosong',
            'tempat_lahir_ayah.required'=>'Tidak Boleh Kosong',
            'tgl_lahir_ayah.required'=>'Harus Pilih',
            'bulan_lahir_ayah.required'=>'Harus Pilih',
            'tahun_lahir_ayah.required'=>'Tidak Boleh Kosong',
            'agama_ayah_id.required'=>'Tidak Boleh Kosong',
            'pendiddikan_ayah_id.required'=>'Tidak Boleh Kosong',
            'pekerjaan_ayah.required'=>'Tidak Boleh Kosong',
            'warga_negara_ayah.required'=>'Tidak Boleh Kosong',
            'nomor_telpon_ayah.required'=>'Tidak Boleh Kosong',
            'nomor_telpon_ayah.unique'=>'Nomor Telp Sudah digunakan',
            'pedapatan_ayah.required'=>'Tidak Boleh Kosong',
            'nama_ibu.required'=>'Tidak Boleh Kosong',
            'tempat_lahir_ibu.required'=>'Tidak Boleh Kosong',
            'tgl_lahir_ibu.required'=>'Harus Pilih',
            'bulan_lahir_ibu.required'=>'Harus Pilih',
            'tahun_lahir_ibu.required'=>'Tidak Boleh Kosong',
            'agama_ibu_id.required'=>'Tidak Boleh Kosong',
            'pendiddikan_ibu_id.required'=>'Tidak Boleh Kosong',
            'pekerjaan_ibu.required'=>'Tidak Boleh Kosong',
            'warga_negara_ibu.required'=>'Tidak Boleh Kosong',
            'nomor_telpon_ibu.required'=>'Tidak Boleh Kosong',
            'nomor_telpon_ibu.unique'=>'Nomor Telp Sudah digunakan',
            'pedapatan_ibu.required'=>'Tidak Boleh Kosong',
            'status_id.required'=>'Tidak Boleh Kosong',
            'admin.required'=>'Tidak Boleh Kosong',
            'aktif.required'=>'Tidak Boleh Kosong',
            'password.required'=>'Tidak Boleh Kosong',
            'password.min'=>'Minimal 6 digit',
            'password.confirmed'=>'Ulangi Kata Sandi tidak sesuai'
          ];
          $this->validate($request, [
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'=>'required', 
            // 'email'=>'required',
            'password'=>'required|string|min:6|confirmed',
            'hp'=>'required|min:10',
            'nama_panggilan'=>'required',
            'jenis_kelamin_id'=>'required',
            'tempat_lahir'=>'required',
            'tgl_lahir'=>'required',
            'bulan_lahir'=>'required',
            'tahun_lahir'=>'required',
            'alamat_lengkap'=>'required',
            'sekolah_asal'=>'required',
            'alamat_sekolah_asal'=>'required',
            'jumlah_saudara_kandung'=>'required',
            'jumlah_saudara_tiri'=>'required',
            'jumlah_saudara_angkat'=>'required',
            'yatim_piyatu_id'=>'required',
            'bahasa_sehari_hari'=>'required',
            'bangsa_negara'=>'required',
            'kelainan_jasmani'=>'required',
            'penyakit'=>'required',
            'tinggi_badan'=>'required',
            'berat_badan'=>'required',
            'prestasi'=>'required',
            'nama_ayah'=>'required',
            'tempat_lahir_ayah'=>'required',
            'tgl_lahir_ayah'=>'required',
            'bulan_lahir_ayah'=>'required',
            'tahun_lahir_ayah'=>'required',
            'agama_ayah_id'=>'required',
            'pendiddikan_ayah_id'=>'required',
            'pekerjaan_ayah'=>'required',
            'warga_negara_ayah'=>'required',
            'nomor_telpon_ayah'=>'required|min:10',
            'pedapatan_ayah'=>'required',
            'nama_ibu'=>'required',
            'tempat_lahir_ibu'=>'required',
            'tgl_lahir_ibu'=>'required',
            'bulan_lahir_ibu'=>'required',
            'tahun_lahir_ibu'=>'required',
            'agama_ibu_id'=>'required',
            'pendiddikan_ibu_id'=>'required',
            'pekerjaan_ibu'=>'required',
            'warga_negara_ibu'=>'required',
            'nomor_telpon_ibu'=>'required|min:10',
            'pedapatan_ibu'=>'required'
          ],$message);
          $dates_lahir = $request->tgl_lahir.'-'.$request->bulan_lahir.'-'.$request->tahun_lahir;
          $tgl_lahir = date('Y-m-d', strtotime($dates_lahir));
          $dates_lahir_ayah = $request->tgl_lahir_ayah.'-'.$request->bulan_lahir_ayah.'-'.$request->tahun_lahir_ayah;
          $tgl_lahir_ayah = date('Y-m-d', strtotime($dates_lahir_ayah));
          $dates_lahir_ibu = $request->tgl_lahir_ibu.'-'.$request->bulan_lahir_ibu.'-'.$request->tahun_lahir_ibu;
          $tgl_lahir_ibu = date('Y-m-d', strtotime($dates_lahir_ibu));

          DB::beginTransaction();
          try {
              $user = User::find($request->user()->id);
              if ($request->file('foto')) {
                if ($user->foto =='null' || $user->foto =='') {
                    $image = $request->file('foto');
                    $imageName = $image->getClientOriginalName();
                    $fileName = date('YmdHis')."_".$imageName;
                    $directory = public_path('/images/siswa/');
                    $imageUrl = $directory.$fileName;
                    Image::make($image)->resize(180, 210)->save($imageUrl);
                }else {
                    $destination_foto =public_path('/images/siswa/'.$user->foto);
                    if(file_exists($destination_foto)){
                                unlink($destination_foto);
                    }
                    $image = $request->file('foto');
                    $imageName = $image->getClientOriginalName();
                    $fileName = date('YmdHis')."_".$imageName;
                    $directory = public_path('/images/siswa/');
                    $imageUrl = $directory.$fileName;
                    Image::make($image)->resize(180, 210)->save($imageUrl);
                }  
                $user->foto=$fileName;
              }
              $user->name=$request->name;
              $user->password=Hash::make($request->password);
              $user->hp=$request->hp;
              $user->nama_panggilan=$request->nama_panggilan;
              $user->jenis_kelamin_id=$request->jenis_kelamin_id;
              $user->tempat_lahir=$request->tempat_lahir;
              $user->tgl_lahir=$tgl_lahir;
              $user->alamat_lengkap=$request->alamat_lengkap;
              $user->sekolah_asal=$request->sekolah_asal;
              $user->alamat_sekolah_asal=$request->alamat_sekolah_asal;
              $user->jumlah_saudara_kandung=$request->jumlah_saudara_kandung;
              $user->jumlah_saudara_tiri=$request->jumlah_saudara_tiri;
              $user->jumlah_saudara_angkat=$request->jumlah_saudara_angkat;
              $user->yatim_piyatu_id=$request->yatim_piyatu_id;
              $user->bahasa_sehari_hari=$request->bahasa_sehari_hari;
              $user->bangsa_negara=$request->bangsa_negara;
              $user->kelainan_jasmani=$request->kelainan_jasmani;
              $user->penyakit=$request->penyakit;
              $user->tinggi_badan=$request->tinggi_badan;
              $user->berat_badan=$request->berat_badan;
              $user->prestasi=$request->prestasi;
              $user->nama_ayah=$request->nama_ayah;
              $user->tempat_lahir_ayah=$request->tempat_lahir_ayah;
              $user->tgl_lahir_ayah=$tgl_lahir_ayah;
              $user->agama_ayah_id=$request->agama_ayah_id;
              $user->pendiddikan_ayah_id=$request->pendiddikan_ayah_id;
              $user->pekerjaan_ayah=$request->pekerjaan_ayah;
              $user->warga_negara_ayah=$request->warga_negara_ayah;
              $user->nomor_telpon_ayah=$request->nomor_telpon_ayah;
              $user->pedapatan_ayah=$request->pedapatan_ayah;
              $user->nama_ibu=$request->nama_ibu;
              $user->tempat_lahir_ibu=$request->tempat_lahir_ibu;
              $user->tgl_lahir_ibu=$tgl_lahir_ibu;
              $user->agama_ibu_id=$request->agama_ibu_id;
              $user->pendiddikan_ibu_id=$request->pendiddikan_ibu_id;
              $user->pekerjaan_ibu=$request->pekerjaan_ibu;
              $user->warga_negara_ibu=$request->warga_negara_ibu;
              $user->nomor_telpon_ibu=$request->nomor_telpon_ibu;
              $user->pedapatan_ibu=$request->pedapatan_ibu;
              $user->update();
          } catch (\Throwable $th) {
            Log::info('gagal Edit Daftar:'.$th->getMessage());
            DB::rollback();
            flash('Maaf! Pendaftaran gagal diedit.')->error();
            return redirect()->back();
          }  
        DB::commit();
        flash('Pendaftaran Berhasil diedit')->important();
        Auth::guard('user')->attempt(['hp' => $request->hp, 'password' => $request->password]);
        return redirect()->intended('/siswa');
    }
    public function alur(Request $request){
        $datas  = AlurPendaftaran::where('is_active','yes')->get();
        return view('siswa.alur_pendaftaran',compact('datas'));
    }
}
