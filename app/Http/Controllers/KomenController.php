<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\UserKomen;
use App\KomentarBerita;
use DB;
use Session;

class KomenController extends Controller
{
    public function __construct()
    {
        $this->middleware('komen');
    }
    
    public function kirim(Request $request,$tgl,$judul){
        $message = [
            'komentar.required'=>'Tidak boleh kosong',
          ];
          $this->validate($request, [
            'komentar' => 'required|min:6|string'
          ],$message );
          DB::beginTransaction();
          try {
            $add = KomentarBerita::create([
                'beritas_id'=>$tgl,
                'komentar'=>$request->komentar,
                'userkomen_id'=>Auth::guard('komen')->user()->id,
                'type'=>'komen',
                'is_active'=>'yes'
            ]);
          } catch (\Throwable $th) {
            Log::info('Gagal Daftar:'.$th->getMessage());
            DB::rollback();
            flash('Maaf! Komentar gagal dikirim')->error();
            return redirect()->back();
          }
        DB::commit();
        flash('Komentar berhasil dikirim')->important();
        return redirect()->back();
    }
}
