<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\UserKomen;
use App\KomentarBerita;
use App\Admin;
use App\User;
use App\Gallery;
use App\Guru;
use App\Karyawan;
use App\Petugas;
use App\Berita;
use App\TanggalPendaftaran;
use Illuminate\Support\Facades\URL;
use Log;
use DB;
use Image;

class LoginController extends Controller
{
  public function index()
  {
    return view('baru_depan');
  }
  public function guru()
  {
    $gurus = Guru::where('is_active','yes')->get();
    return view('guru',compact('gurus'));
  }
  public function detailguru($id){
    $gurus = Guru::where('is_active','yes')->where('id',$id)->first();
    return view('detail_guru',compact('gurus'));
  }
  public function karyawan()
  {
    $karyawans = Petugas::where('is_active','yes')->get();
    return view('karyawan',compact('karyawans'));
  }
  public function gallery()
  {
    $galleries = Gallery::where('is_active','yes')->get();
    return view('gallery',compact('galleries'));
  }
  public function kursus(){
    return view('kursus');
  }
  public function detailkursus($id){
    return view('single_kursus');
  }
  public function blog()
  {
    $datas  = Berita::get();
    
    return view('blog',compact('datas'));
  }
  public function detailblog(Request $request, $tgl , $judul)
  {
    $juduls = str_replace('-',' ',$judul);
    $data  = Berita::where('id',$tgl)->where('judul',$juduls)->first();
    return view('detail_blog',compact('data'));
  }
  public function kontak()
  {
    return view('kontak');
  }
  public function sejarahsekolah(){
    return view('sejarah_sekolah');
  }
  public function visimisi(){
    return view('visi_misi');
  }
  public function strukturorganisasi(){
    return view('struktur_organisasi');
  }
  public function kultursekolah(){
    return view('kultur_sekolah');
  }
  public function layananpendidikan(){
    return view('layanan_pendidikan');
  }
  public function ruangkelas(){
    return view('ruang_kelas');
  }
  public function ruangperpustakaan(){
    return view('ruang_perpustakaan');
  }
  public function laboraturiumn(){
    return view('ruang_laboraturiumn');
  }
  public function aulasekolah(){
    return view('aula_sekolah');
  }
  public function mesjid(){
    return view('mesjid');
  }
  public function frontoffice(){
    return view('front_office');
  }
  public function kantinsekolah(){
    return view('kantin_sekolah');
  }
  public function tamansekolah(){
    return view('taman_sekolah');
  }
  public function toilet(){
    return view('toilet');
  }
  public function bussekolah(){
    return view('bus_sekolah');
  }

  public function kurikulum(){
    $akademiks = 'E-Kurikulum';
    return view('akademik',compact('akademiks'));
  }
  public function penilaian(){
    $akademiks = 'E-Penilaian';
    return view('akademik',compact('akademiks'));
  }
  public function rapor(){
    $akademiks = 'E-Rapor';
    return view('akademik',compact('akademiks'));
  }
  public function dapodik(){
    $akademiks = 'Dapodik';
    return view('akademik',compact('akademiks'));
  }
  public function lulus(){
    $akademiks = 'E-Lulus';
    return view('akademik',compact('akademiks'));
  }
  public function learning(){
    $akademiks = 'E-Learning';
    return view('akademik',compact('akademiks'));
  }

  public function daftar()
    {
      return view('daftar');
    }
    public function getLogin()
    {
      return view('masuk');
    }
    public function postLogin(Request $request)
    {
        // Validate the form data
        // return $request->all();
      $message = [
        'hp.required'=>'Tidak boleh kosong',
        'password.required'=>'Tidak boleh kosong',
        'hp.min'=>'Minimal 10 Digit',
        'hp.numeric'=>'Harus Angka',
        'password.min'=>'Minimal 6 Digit',
      ];
      $this->validate($request, [
        'hp' => 'required|min:10|numeric',
        'password' => 'required|min:6|string'
      ],$message );
      if (Auth::guard('admin')->attempt(['hp' => $request->hp, 'password' => $request->password])) {
        flash('Selamat Datang '.Auth::guard('admin')->user()->name.'...')->important();
        return redirect()->intended('/admin');
      } else if (Auth::guard('user')->attempt(['hp' => $request->hp, 'password' => $request->password])) {
        flash('Selamat Datang '.Auth::guard('user')->user()->name.'...')->important();
        return redirect()->intended('/siswa');
      }else if (Auth::guard('petugas')->attempt(['hp' => $request->hp, 'password' => $request->password])) {
        flash('Selamat Datang '.Auth::guard('petugas')->user()->name.'...')->important();
        return redirect()->intended('/petugas');
      }else if (Auth::guard('guru')->attempt(['hp' => $request->hp, 'password' => $request->password])) {
        flash('Selamat Datang '.Auth::guard('guru')->user()->name.'...')->important();
        return redirect()->intended('/guru');
      }else {
        flash('Nomor Hp atau Kata Sandi Salah, silahkan ulangi kembali.')->error();
        return redirect()->route('masuk');
      }
    }
    public function logout($guards)
    {
      if ($guards == 'komen') {
        Auth::guard('komen')->logout();
        return redirect()->back();
      }else {
        Auth::guard($guards)->logout();
      }
      return redirect('/');
    }
    public function postdaftar(Request $request){
      $tanggal = TanggalPendaftaran::find(1);
      if (date('Y-m-d') < $tanggal->mulai || date('Y-m-d') > $tanggal->selesai) {
        flash('Maaf, Pendaftaran hanya di buka Tgl '.date('d-m-Y', strtotime($tanggal->mulai)).' S/d '.date('d-m-Y', strtotime($tanggal->selesai)) )->error();
        return redirect()->back();
      }
      $message = [
        'name.required'=>'Tidak Boleh Kosong', 
        'foto.required'=>'Foto Wajib Masukan',
        'foto.image'=>'Format gambar',
        'password.required'=>'Tidak Boleh Kosong',
        'hp.required'=>'Tidak Boleh Kosong',
        'hp.unique'=>'Nomor Telp Sudah digunakan',
        'hp.min'=>'Nomor Telp Minimal 10 digit',
        'nama_panggilan.required'=>'Tidak Boleh Kosong',
        'jenis_kelamin_id.required'=>'Tidak Boleh Kosong',
        'tempat_lahir.required'=>'Tidak Boleh Kosong',
        'tgl_lahir.required'=>'Harus Pilih',
        'bulan_lahir.required'=>'Harus Pilih',
        'tahun_lahir.required'=>'Harus Pilih',
        'alamat_lengkap.required'=>'Tidak Boleh Kosong',
        'sekolah_asal.required'=>'Tidak Boleh Kosong',
        'alamat_sekolah_asal.required'=>'Tidak Boleh Kosong',
        'jumlah_saudara_kandung.required'=>'Tidak Boleh Kosong',
        'jumlah_saudara_tiri.required'=>'Tidak Boleh Kosong',
        'jumlah_saudara_angkat.required'=>'Tidak Boleh Kosong',
        'yatim_piyatu_id.required'=>'Tidak Boleh Kosong',
        'bahasa_sehari_hari.required'=>'Tidak Boleh Kosong',
        'bangsa_negara.required'=>'Tidak Boleh Kosong',
        'kelainan_jasmani.required'=>'Tidak Boleh Kosong',
        'penyakit.required'=>'Tidak Boleh Kosong',
        'tinggi_badan.required'=>'Tidak Boleh Kosong',
        'berat_badan.required'=>'Tidak Boleh Kosong',
        'prestasi.required'=>'Tidak Boleh Kosong',
        'nama_ayah.required'=>'Tidak Boleh Kosong',
        'tempat_lahir_ayah.required'=>'Tidak Boleh Kosong',
        'tgl_lahir_ayah.required'=>'Harus Pilih',
        'bulan_lahir_ayah.required'=>'Harus Pilih',
        'tahun_lahir_ayah.required'=>'Tidak Boleh Kosong',
        'agama_ayah_id.required'=>'Tidak Boleh Kosong',
        'pendiddikan_ayah_id.required'=>'Tidak Boleh Kosong',
        'pekerjaan_ayah.required'=>'Tidak Boleh Kosong',
        'warga_negara_ayah.required'=>'Tidak Boleh Kosong',
        'nomor_telpon_ayah.required'=>'Tidak Boleh Kosong',
        'nomor_telpon_ayah.unique'=>'Nomor Telp Sudah digunakan',
        'pedapatan_ayah.required'=>'Tidak Boleh Kosong',
        'nama_ibu.required'=>'Tidak Boleh Kosong',
        'tempat_lahir_ibu.required'=>'Tidak Boleh Kosong',
        'tgl_lahir_ibu.required'=>'Harus Pilih',
        'bulan_lahir_ibu.required'=>'Harus Pilih',
        'tahun_lahir_ibu.required'=>'Tidak Boleh Kosong',
        'agama_ibu_id.required'=>'Tidak Boleh Kosong',
        'pendiddikan_ibu_id.required'=>'Tidak Boleh Kosong',
        'pekerjaan_ibu.required'=>'Tidak Boleh Kosong',
        'warga_negara_ibu.required'=>'Tidak Boleh Kosong',
        'nomor_telpon_ibu.required'=>'Tidak Boleh Kosong',
        'nomor_telpon_ibu.unique'=>'Nomor Telp Sudah digunakan',
        'pedapatan_ibu.required'=>'Tidak Boleh Kosong',
        'status_id.required'=>'Tidak Boleh Kosong',
        'admin.required'=>'Tidak Boleh Kosong',
        'aktif.required'=>'Tidak Boleh Kosong',
        'password.min'=>'Minimal 6 digit',
        'password.confirmed'=>'Kata Sandi tidak sesuai'
      ];
      $this->validate($request, [
        'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'name'=>'required', 
        // 'email'=>'required',
        'password'=>'required|string|min:6|confirmed',
        'hp'=>'required|unique:users|unique:admins|unique:petugas|unique:gurus|min:10',
        'nama_panggilan'=>'required',
        'jenis_kelamin_id'=>'required',
        'tempat_lahir'=>'required',
        'tgl_lahir'=>'required',
        'bulan_lahir'=>'required',
        'tahun_lahir'=>'required',
        'alamat_lengkap'=>'required',
        'sekolah_asal'=>'required',
        'alamat_sekolah_asal'=>'required',
        'jumlah_saudara_kandung'=>'required',
        'jumlah_saudara_tiri'=>'required',
        'jumlah_saudara_angkat'=>'required',
        'yatim_piyatu_id'=>'required',
        'bahasa_sehari_hari'=>'required',
        'bangsa_negara'=>'required',
        'kelainan_jasmani'=>'required',
        'penyakit'=>'required',
        'tinggi_badan'=>'required',
        'berat_badan'=>'required',
        'prestasi'=>'required',
        'nama_ayah'=>'required',
        'tempat_lahir_ayah'=>'required',
        'tgl_lahir_ayah'=>'required',
        'bulan_lahir_ayah'=>'required',
        'tahun_lahir_ayah'=>'required',
        'agama_ayah_id'=>'required',
        'pendiddikan_ayah_id'=>'required',
        'pekerjaan_ayah'=>'required',
        'warga_negara_ayah'=>'required',
        'nomor_telpon_ayah'=>'required|unique:users|unique:admins|unique:petugas|unique:gurus',
        'pedapatan_ayah'=>'required',
        'nama_ibu'=>'required',
        'tempat_lahir_ibu'=>'required',
        'tgl_lahir_ibu'=>'required',
        'bulan_lahir_ibu'=>'required',
        'tahun_lahir_ibu'=>'required',
        'agama_ibu_id'=>'required',
        'pendiddikan_ibu_id'=>'required',
        'pekerjaan_ibu'=>'required',
        'warga_negara_ibu'=>'required',
        'nomor_telpon_ibu'=>'required|unique:users|unique:admins|unique:petugas|unique:gurus',
        'pedapatan_ibu'=>'required'
      ],$message);
      $dates_lahir = $request->tgl_lahir.'-'.$request->bulan_lahir.'-'.$request->tahun_lahir;
      $tgl_lahir = date('Y-m-d', strtotime($dates_lahir));
      $dates_lahir_ayah = $request->tgl_lahir_ayah.'-'.$request->bulan_lahir_ayah.'-'.$request->tahun_lahir_ayah;
      $tgl_lahir_ayah = date('Y-m-d', strtotime($dates_lahir_ayah));
      $dates_lahir_ibu = $request->tgl_lahir_ibu.'-'.$request->bulan_lahir_ibu.'-'.$request->tahun_lahir_ibu;
      $tgl_lahir_ibu = date('Y-m-d', strtotime($dates_lahir_ibu));
      if ($cek_nomor = User::orderBy('nomor_pendaftaran','DESC')->first()) {
        $nomor = (int)$cek_nomor->nomor_pendaftaran+1;
        $sequence = substr($nomor,-6);
        $no_pendaftaran = 'DFTR-'.date('ym').$sequence;
      } else {
        $no_pendaftaran = 'DFTR-'.date('ym000000');
      }
      
      
      
      DB::beginTransaction();
      try {
        $image = $request->file('foto');
        $imageName = $image->getClientOriginalName();
        $fileName = date('YmdHis')."_".$imageName;
        $directory = public_path('/images/siswa/');
        $imageUrl = $directory.$fileName;
        Image::make($image)->resize(180, 210)->save($imageUrl);

        $users = User::create([
          'name'=>$request->name, 
          'foto'=>$fileName,
          'nomor_pendaftaran'=>$no_pendaftaran,
          'password'=>Hash::make($request->password),
          'hp'=>$request->hp,
          'nama_panggilan'=>$request->nama_panggilan,
          'jenis_kelamin_id'=>$request->jenis_kelamin_id,
          'tempat_lahir'=>$request->tempat_lahir,
          'tgl_lahir'=>$tgl_lahir,
          'alamat_lengkap'=>$request->alamat_lengkap,
          'sekolah_asal'=>$request->sekolah_asal,
          'alamat_sekolah_asal'=>$request->alamat_sekolah_asal,
          'jumlah_saudara_kandung'=>$request->jumlah_saudara_kandung,
          'jumlah_saudara_tiri'=>$request->jumlah_saudara_tiri,
          'jumlah_saudara_angkat'=>$request->jumlah_saudara_angkat,
          'yatim_piyatu_id'=>$request->yatim_piyatu_id,
          'bahasa_sehari_hari'=>$request->bahasa_sehari_hari,
          'bangsa_negara'=>$request->bangsa_negara,
          'kelainan_jasmani'=>$request->kelainan_jasmani,
          'penyakit'=>$request->penyakit,
          'tinggi_badan'=>$request->tinggi_badan,
          'berat_badan'=>$request->berat_badan,
          'prestasi'=>$request->prestasi,
          'nama_ayah'=>$request->nama_ayah,
          'tempat_lahir_ayah'=>$request->tempat_lahir_ayah,
          'tgl_lahir_ayah'=>$tgl_lahir_ayah,
          'agama_ayah_id'=>$request->agama_ayah_id,
          'pendiddikan_ayah_id'=>$request->pendiddikan_ayah_id,
          'pekerjaan_ayah'=>$request->pekerjaan_ayah,
          'warga_negara_ayah'=>$request->warga_negara_ayah,
          'nomor_telpon_ayah'=>$request->nomor_telpon_ayah,
          'pedapatan_ayah'=>$request->pedapatan_ayah,
          'nama_ibu'=>$request->nama_ibu,
          'tempat_lahir_ibu'=>$request->tempat_lahir_ibu,
          'tgl_lahir_ibu'=>$tgl_lahir_ibu,
          'agama_ibu_id'=>$request->agama_ibu_id,
          'pendiddikan_ibu_id'=>$request->pendiddikan_ibu_id,
          'pekerjaan_ibu'=>$request->pekerjaan_ibu,
          'warga_negara_ibu'=>$request->warga_negara_ibu,
          'nomor_telpon_ibu'=>$request->nomor_telpon_ibu,
          'pedapatan_ibu'=>$request->pedapatan_ibu,
          'status_id'=>1,
          'petugas_id'=>1,
          'aktif'=>'yes'
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal Daftar:'.$e->getMessage());
        DB::rollback();
        flash('Maaf! Pendaftaran gagal, silahkan ulangi kembali.')->error();
        return view('daftar');
      }
      DB::commit();
      flash('Pendaftaran Berhasil')->important();
      Auth::guard('user')->attempt(['hp' => $request->hp, 'password' => $request->password]);
      return redirect()->intended('/siswa');

    }
    public function print(){
      return view('export.bukti_pendaftaran');
    }

    //LOGIN SOSMED
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    
    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request, $provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::guard('komen')->attempt(['provider_id'=>$user->id,'password'=>'facebook']);
        $link = $request->session()->get('tgl')."/".$request->session()->get('judul');
        
        return redirect()->back();
    }
    
    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = UserKomen::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        else{
            $data = UserKomen::create([
                'name'     => $user->name,
                'email'    => !empty($user->email)? $user->email : '' ,
                'password'=>Hash::make($provider),
                'provider' => $provider,
                'provider_id' => $user->id,
                'type'=>'komen'
                ]);
            return $data;
        }
    }
    //END LOGIN SOSMED

    public function commingson(){
      flash('Maaf! Halaman ini masih dalam perbaikan.')->error();
      return redirect()->back();
    }

}
