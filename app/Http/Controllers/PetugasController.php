<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Gallery;
use App\Guru;
use App\Karyawan;
use App\Berita;
use App\Petugas;
use App\AlurPendaftaran;
use Image;
use DB;
use Log;

class PetugasController extends Controller
{
    public function __construct()
    {
        $this->middleware('petugas');
    }
    public function index(){
        return view('petugas.index');
    }
    public function gallery(Request $request){
        if($request->hasFile('gambar')){
           
            $this->validate($request, [
                'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'kategori'=>'required'
            ]);

            DB::beginTransaction();
            try {
                
                $image = $request->file('gambar');
                $imageName = $image->getClientOriginalName();
                $fileName = date('YmdHis')."_".$imageName;
                $directory = public_path('/images/galleries/');
                $imageUrl = $directory.$fileName;
                Image::make($image)->resize(450, 250)->save($imageUrl);
                $gallery = Gallery::create([
                    'gambar'=>$fileName,
                    'kategori'=>$request->kategori,
                    'is_active'=>'yes',
                    'petugas_id'=>$request->user()->id
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal Edit Profil:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Gagal disimpan.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Berhasil disimpan')->important();
            return redirect()->back();
        }
        $galleries = Gallery::where('is_active','yes')->get();
        return view('petugas.gallery',compact('galleries'));
    }

    public function guru(Request $request){
        if ($request->action =='add') {
            $this->validate($request, [
                'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'hp'=>'required',
                'email'=>'required',
                'kata_mutiara'=>'required',
                'nama'=>'required',
                'kategori'=>'required'
            ]);
            DB::beginTransaction();
            try {
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = date('YmdHis')."_".$imageName;
                $directory = public_path('/images/guru/');
                $imageUrl = $directory.$fileName;
                Image::make($image)->resize(262, 300)->save($imageUrl);
                Guru::create([
                    'name'=>$request->nama,
                    'hp'=>$request->hp,
                    'email'=>$request->email,
                    'password'=>Hash::make(rand(100000,999999)),
                    'link_fb'=>$request->link_fb,
                    'foto'=>$fileName,
                    'pengalaman'=>$request->pengalaman,
                    'alamat'=>$request->alamat,
                    'kata_mutiara'=>$request->kata_mutiara,
                    'kategori_id'=>$request->kategori,
                    'petugas_id'=>$request->user()->id,
                    'type'=>'guru',
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal Edit Profil:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Gagal disimpan.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Berhasil disimpan')->important();
            return redirect()->back();
        } 
        $gurus = Guru::where('is_active','yes')->get();
        return view('petugas.guru',compact('gurus'));
    }
    public function detailguru($id){
        $gurus = Guru::where('is_active','yes')->where('id',$id)->first();
        return view('petugas.detail_guru',compact('gurus'));
    }
    public function karyawan(Request $request){
        if ($request->action =='add') {
            $message = [
                'nama.required'=>'Nama wajib di isi',
                'nama.min'=>'Nama minimal 3 huruf',
                'foto.required'=>'Foto wajib di masukan',
            ];
            $this->validate($request, [
                'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'nama'=>'required|min:3',
            ],$message);
            DB::beginTransaction();
            try {
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = date('YmdHis')."_".$imageName;
                $directory = public_path('/images/karyawan/');
                $imageUrl = $directory.$fileName;
                Image::make($image)->resize(262, 300)->save($imageUrl);
                Petugas::create([
                    'name'=>$request->nama,
                    'link_fb'=>$request->link_fb,
                    'foto'=>$fileName,
                    'link_fb'=>$request->link_fb,
                    'instagram'=>$request->instagram,
                    'petugas_id'=>$request->user()->id
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal Edit Profil:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Gagal disimpan.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Berhasil disimpan')->important();
            return redirect()->back();
        } 
        $karyawan = Petugas::where('is_active','yes')->get();
        return view('petugas.karyawan',compact('karyawan'));
    }
    public function pengaturan(Request $request){
        return view('petugas.pengaturan');
    }
    public function berita(Request $request){
        $datas = Berita::where('is_active','yes')->get();
        return view('petugas.berita',compact('datas'));
    }
    public function formberita(Request $request){
        return view('petugas.form_berita');
    }
    public function createberita(Request $request){
        $message = [
            'judul.required'=>'Nama wajib di isi',
            'judul.min'=>'Nama minimal 3 karakter',
            'isi.required'=>'Nama wajib di isi',
            'isi.min'=>'Nama minimal 100 karakter',
            'gambar.required'=>'Foto wajib di masukan',
            'gambar.max'=>'Ukuran gambar terlalu besar'
        ];
        $this->validate($request, [
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'judul'=>'required|min:3',
            'isi'=>'required|min:1'
        ],$message);
        DB::beginTransaction();
        try {
            $image = $request->file('gambar');
            $imageName = $image->getClientOriginalName();
            $fileName = date('YmdHis')."_".$imageName;
            $directory = public_path('/images/blog/');
            $imageUrl = $directory.$fileName;
            Image::make($image)->resize(1920, 1280)->save($imageUrl);

            $add = Berita::create([
                'judul'=>$request->judul,
                'isi'=>nl2br($request->isi),
                'tgl_posting'=>date('Y-m-d'),
                'gambar'=>$fileName,
                'like'=>0,
                'petugas_id'=>$request->user()->id,
                'is_active'=>'yes'
            ]);
        } catch (\Throwable $th) {
            Log::info('Gagal Edit Profil:'.$th->getMessage());
            DB::rollback();
            flash('Maaf! berita Gagal disimpan.')->error();
            return redirect()->back();
        }
        DB::commit();
        flash('Berita Berhasil disimpan')->important();
        return redirect()->back();
    }
    public function alurpendaftaran(Request $request){ 
        if($request->judul){
            $message = [
                'judul.required'=>'Nama wajib di isi',
                'judul.min'=>'Nama minimal 3 karakter',
                'tgl_mulai.required'=>'Tangal Wajib Diisi',
                'tgl_selesai.required'=>'Tangal Wajib Diisi',
                'penjelasan.required'=>'Nama wajib di isi',
                'penjelasan.min'=>'Nama minimal 10 karakter',
            ];
            $this->validate($request, [
                'tgl_mulai'=>'required',
                'tgl_selesai'=>'required',
                'judul'=>'required|min:3',
                'penjelasan'=>'required|min:10'
            ],$message);
            DB::beginTransaction();
            try {
                $create = AlurPendaftaran::create([
                    'tgl_mulai'=>date('Y-m-d', strtotime($request->tgl_mulai)),
                    'tgl_selesai'=>date('Y-m-d', strtotime($request->tgl_selesai)),
                    'judul'=>$request->judul,
                    'keterangan'=>$request->penjelasan,
                    'is_active'=>'yes'
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal tambah Alur:'.$th->getMessage());
                DB::rollback();
                flash('Maaf! Alur Gagal disimpan.')->error();
                return redirect()->back();
            }
            DB::commit();
            flash('Alur Berhasil disimpan')->important();
            return redirect()->back();

        }
        $datas  = AlurPendaftaran::where('is_active','yes')->get();
        return view('petugas.alur_pendaftaran',compact('datas'));
    }
    public function calonsiswa(){
        $datas = User::where('aktif','yes')->get();
        return view('petugas.data_siswa_terdaftar',compact('datas'));
    }
}
