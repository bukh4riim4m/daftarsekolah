<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlurPendaftaran extends Model
{
    protected $fillable = [
        'judul','keterangan', 'tgl_mulai','tgl_selesai','is_active'
    ];
}
