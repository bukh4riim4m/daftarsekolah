<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarBerita extends Model
{
    protected $fillable = [
        'beritas_id', 'userkomen_id','komentar','is_active'
    ];
    public function userBeritaId(){
        return $this->belongsTo('App\UserKomen','userkomen_id');
    }
}
