<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $fillable = [
        'name','link_fb','instagram','foto','petugas_id','is_active'
    ];
}
