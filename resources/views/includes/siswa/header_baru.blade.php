  <!-- Header -->
  <?php $banner = App\Banner::find(1);?>
  <header id="header" class="header layer-overlay overlay-dark-deep" data-bg-color="#333">
    <div class="header-nav">
      <div class="header-nav-wrapper">
        <div class="container-fluid p-0">
          <div id="menuzord-verticalnav" class="menuzord" style="background:#222222;">
            <a class="menuzord-brand p-20" href="{{url('/')}}"><img width="165px" alt="logo" src="{{asset('images/logo_sma_1_sape_putih.png')}}"></a>
            <ul class="menuzord-menu">
            <li><a href="{{url('petugas')}}">Home</a></li>
            <li><a href="#data siswa">Data Siswa</a>
                <ul class="dropdown">
                  <li><a href="page-teachers-style1.html">Calon Siswa</a></li>
                  <li><a href="page-teachers-style2.html">Siswa Lolos</a></li>
                  <li><a href="page-teachers-details.html">Siswa Kelas 1</a></li>
                  <li><a href="page-teachers-details.html">Siswa Kelas 2</a></li>
                  <li><a href="page-teachers-details.html">Siswa Kelas 3</a></li>
                </ul>
              </li>
            <li class="{{setActive(['petugas-gallery','petugas-guru'])}}"><a href="#">Halaman</a>
                <ul class="dropdown">
                  <li><a href="#">Profil Sekolah</a>
                    <ul class="dropdown">
                      <li><a href="">Sejarah</a></li>
                      <li><a href="">Visi Misi</a></li>
                      <li><a href="">Struktur Organisasi</a></li>
                      <li><a href="">Kultur Sekolah</a></li>
                      <li><a href="">Layanan Pendidikan</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Fasilitas</a>
                    <ul class="dropdown">
                        <li><a href="">Ruang Kelas</a></li>
                      <li><a href="">Ruang Perpustakaan</a></li>
                      <li><a href="">Laboraturium</a></li>
                      <li><a href="">Aula Sekolah</a></li>
                      <li><a href="">Mesjid</a></li>
                      <li><a href="">Front Office</a></li>
                      <li><a href="">Kantin Sekolah</a> </li>
                      <li><a href="">Taman Sekolah</a></li>
                      <li><a href="">Toilet</a></li>
                      <li><a href="">Bus Sekolah</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Akademik</a>
                    <ul class="dropdown">
                        <li><a href="">E-Kurikulum</a></li>
                        <li><a href="">E-Penilaian</a></li>
                        <li><a href="">E-Rapor</a></li>
                        <li><a href="">Dapodik</a></li>
                        <li><a href="">Si-Lulus</a></li>
                        <li><a href="">E-Learning</a></li>
                        <li><a href="">Kursus</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Guru & Karyawan</a>
                    <ul class="dropdown">
                      <li><a href="{{route('petugas-guru')}}">Guru</a></li>
                      <li><a href="">Karyawan</a></li>
                    </ul>
                  </li>
                  <li><a href="{{route('petugas-gallery')}}">Gallery</a></li>
                  <li><a href="">Berita</a></li>
                  <li><a href="">Kontak</a></li>
                </ul>
              </li>
              <li><a href="{{route('pengaturan')}}">Pengaturan</a></li>
              <li><a href="{{url('logout')}}">Logout</a></li>
            
            </ul>
          </div>
          <div class="clearfix"></div>
          <div class="vertical-nav-widget p-30 pt-10">
            <div class="widget no-border">
              <ul>
                <li class="font-14 mb-5"> <i class="fa fa-phone text-theme-color-2"></i> <a class="text-gray" href="#">{{$banner->telpon}}</a> </li>
                <li class="font-14 mb-5"> <i class="fa fa-clock-o text-theme-color-2"></i> Mon-Fri 8:00 to 2:00 </li>
                <li class="font-14 mb-5"> <i class="fa fa-envelope-o text-theme-color-2"></i> <a class="text-gray" href="#">{{$banner->email}}</a> </li>
              </ul>      
            </div>
            <div class="widget">
              <ul class="styled-icons icon-dark icon-theme-colored icon-sm">
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              </ul>
            </div>
            <p>Copyright &copy;2019 Developer by IMAM BUKHARI</p>
          </div>
        </div>
      </div>
    </div>
  </header>