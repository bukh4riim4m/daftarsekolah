  <!-- Header -->
  <?php $banner = App\Banner::find(1);?>
  <header id="header" class="header layer-overlay overlay-dark-deep" data-bg-color="#333">
    <div class="header-nav">
      <div class="header-nav-wrapper">
        <div class="container-fluid p-0">
          <div id="menuzord-verticalnav" class="menuzord" style="background:#222222;">
            <a class="menuzord-brand p-20" href="{{url('/')}}"><img width="165px" alt="logo" src="{{asset('images/logo_sma_1_sape_putih.png')}}"></a>
            <ul class="menuzord-menu">
            <li><a href="{{url('petugas')}}">Home</a></li>
            <li class="{{setActive(['petugas-calon-siswa-terdaftar','petugas-karyawan','petugas-guru'])}}"><a href="#">Data Master</a>
                <ul class="dropdown">
                  <li><a href="{{route('petugas-calon-siswa-terdaftar')}}">Calon Siswa Terdaftar</a></li>
                  <li><a href="{{route('commingson')}}">Calon Siswa Terverifikasi</a></li>
                  <li><a href="{{route('commingson')}}">Data Siswa Lulus</a></li>
                  <li><a href="{{route('petugas-karyawan')}}">Data Karyawan</a></li>
                  <li><a href="{{route('petugas-guru')}}">Data Guru</a></li>
                </ul>
              </li>
            <li class="{{setActive(['petugas-gallery'])}}"><a href="#">Halaman</a>
                <ul class="dropdown">
                  <li><a href="#">Profil Sekolah</a>
                    <ul class="dropdown">
                      <li><a href="{{route('commingson')}}">Sejarah</a></li>
                      <li><a href="{{route('commingson')}}">Visi Misi</a></li>
                      <li><a href="{{route('commingson')}}">Struktur Organisasi</a></li>
                      <li><a href="{{route('commingson')}}">Kultur Sekolah</a></li>
                      <li><a href="{{route('commingson')}}">Layanan Pendidikan</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Fasilitas</a>
                    <ul class="dropdown">
                        <li><a href="">Ruang Kelas</a></li>
                      <li><a href="{{route('commingson')}}">Ruang Perpustakaan</a></li>
                      <li><a href="{{route('commingson')}}">Laboraturium</a></li>
                      <li><a href="{{route('commingson')}}">Aula Sekolah</a></li>
                      <li><a href="{{route('commingson')}}">Mesjid</a></li>
                      <li><a href="{{route('commingson')}}">Front Office</a></li>
                      <li><a href="{{route('commingson')}}">Kantin Sekolah</a> </li>
                      <li><a href="{{route('commingson')}}">Taman Sekolah</a></li>
                      <li><a href="{{route('commingson')}}">Toilet</a></li>
                      <li><a href="{{route('commingson')}}">Bus Sekolah</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Akademik</a>
                    <ul class="dropdown">
                        <li><a href="{{route('commingson')}}">E-Kurikulum</a></li>
                        <li><a href="{{route('commingson')}}">E-Penilaian</a></li>
                        <li><a href="{{route('commingson')}}">E-Rapor</a></li>
                        <li><a href="{{route('commingson')}}">Dapodik</a></li>
                        <li><a href="{{route('commingson')}}">Si-Lulus</a></li>
                        <li><a href="{{route('commingson')}}">E-Learning</a></li>
                        <li><a href="{{route('commingson')}}">Kursus</a></li>
                    </ul>
                  </li>
                  <li><a href="{{route('petugas-gallery')}}">Gallery</a></li>
                  <li><a href="{{route('petugas-berita')}}">Berita</a></li>
                  <li><a href="#">Calon Siswa</a>
                    <ul class="dropdown">
                    <li><a href="{{route('petugas-alur-pendaftaran')}}">Alur Pendaftaran</a></li>
                    </ul>
                  </li>
                  
                  <li><a href="">Kontak</a></li>
                </ul>
              </li>
              <li><a href="{{route('petugas-pengaturan')}}">Pengaturan</a></li>
              <li><a href="{{url('/logout/petugas')}}">Logout</a></li>
            
            </ul>
          </div>
          <div class="clearfix"></div>
          <div class="vertical-nav-widget p-30 pt-10">
            <div class="widget no-border">
              <ul>
                <li class="font-14 mb-5"> <i class="fa fa-phone text-theme-color-2"></i> <a class="text-gray" href="#">{{$banner->telpon}}</a> </li>
                <li class="font-14 mb-5"> <i class="fa fa-clock-o text-theme-color-2"></i> Mon-Fri 8:00 to 2:00 </li>
                <li class="font-14 mb-5"> <i class="fa fa-envelope-o text-theme-color-2"></i> <a class="text-gray" href="#">{{$banner->email}}</a> </li>
              </ul>      
            </div>
            <div class="widget">
              <ul class="styled-icons icon-dark icon-theme-colored icon-sm">
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              </ul>
            </div>
            <p>Copyright &copy;2019 Developer by IMAM BUKHARI</p>
          </div>
        </div>
      </div>
    </div>
  </header>