<div class="header">
    <div class="header-left">

        <a href="{{url('/')}}" class="logo">
          <img src="{{asset('assets/images/kmbti_logo.png')}}" width="80" height="50" alt="">
        </a>
        <a href="{{url('/home')}}" class="logo-dark">

          <img src="{{asset('assets/images/kmbti_logo.png')}}"  width="80" height="50" alt="">
        </a>
    </div>
    <div class="page-title-box pull-left">
        <h3>NAMA</h3>
    </div>
<a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
<ul class="nav navbar-nav navbar-right user-menu pull-right">

<li class="dropdown">
<a href="#" class="dropdown-toggle user-link" data-toggle="dropdown">
  <span class="user-img"><img class="avatar" src="{{asset('/foto/'.Auth::user()->fotodiri)}}" width="40">
  <span class="status online"></span></span>
  <span>{{Auth::guard('petugas')->user()->name}}</span>
  <i class="caret"></i>
</a>
<ul class="dropdown-menu">
  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">KELUAR</a></li>
</ul>
</li>
</ul>
<div class="dropdown mobile-user-menu pull-right">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
<ul class="dropdown-menu pull-right">
<li><a href="#" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">KELUAR</a></li>
</ul>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
</div>
</div>
