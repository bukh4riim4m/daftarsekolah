<?php $banner = App\Banner::find(1);?>
<div id="wrapper" class="clearfix">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div> 
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-color-2 sm-text-center p-0">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="widget no-border m-0">
              <ul class="list-inline font-13 sm-text-center mt-5">
                <!-- <li>
                  <a class="text-white" href="#">FAQ</a>
                </li>
                <li class="text-white">|</li> -->
                @if(Auth::guard())
                  @if(Auth::guard('user')->check())
                  <li>
                    <p class="text-white">{{Auth::guard('user')->user()->name}}</p>
                  </li>
                  @elseif(Auth::guard('petugas')->check())
                  <li>
                    <p class="text-white">{{Auth::guard('petugas')->user()->name}}</p>
                  </li>
                  @elseif(Auth::guard('guru')->check())
                  <li>
                    <p class="text-white">{{Auth::guard('guru')->user()->name}}</p>
                  </li>
                  @elseif(Auth::guard('admin')->check())
                  <li>
                    <p class="text-white">{{Auth::guard('admin')->user()->name}}</p>
                  </li>
                  @else 
                  <li>
                    <a class="text-white" href="{{route('daftar')}}">Pendaftaran Sekolah</a>
                  </li>
                  <li class="text-white">|</li>
                  <li>
                    <a class="text-white" href="{{route('masuk')}}">Login</a>
                  </li>
                  @endif
                @else
                <li>
                    <a class="text-white" href="{{route('daftar')}}">Pendaftaran Sekolah</a>
                  </li>
                  <li class="text-white">|</li>
                  <li>
                    <a class="text-white" href="{{route('masuk')}}">Login</a>
                  </li>
                @endif
              </ul>
            </div>
          </div>
          <div class="col-md-8">
            <div class="widget m-0 pull-right sm-pull-none sm-text-center">
              <ul class="list-inline pull-right">
                {{--<li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-cart-link has-dropdown text-white text-hover-theme-colored">
                      <i class="fa fa-shopping-cart font-13"></i> (12)
                    </a>
                    <ul class="dropdown">
                      <li>
                        <!-- dropdown cart -->
                        <div class="dropdown-cart">
                          <table class="table cart-table-list table-responsive">
                            <tbody>
                              <tr>
                                <td><a href="#"><img alt="" src="http://placehold.it/85x85"></a></td>
                                <td><a href="#"> Product Title</a></td>
                                <td>X3</td>
                                <td>$ 100.00</td>
                                <td><a class="close" href="#"><i class="fa fa-close font-13"></i></a></td>
                              </tr>
                              <tr>
                                <td><a href="#"><img alt="" src="http://placehold.it/85x85"></a></td>
                                <td><a href="#"> Product Title</a></td>
                                <td>X2</td>
                                <td>$ 70.00</td>
                                <td><a class="close" href="#"><i class="fa fa-close font-13"></i></a></td>
                              </tr>
                            </tbody>
                          </table>
                          <div class="total-cart text-right">
                            <table class="table table-responsive">
                              <tbody>
                                <tr>
                                  <td>Cart Subtotal</td>
                                  <td>$170.00</td>
                                </tr>
                                <tr>
                                  <td>Shipping and Handling</td>
                                  <td>$20.00</td>
                                </tr>
                                <tr>
                                  <td>Order Total</td>
                                  <td>$190.00</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="cart-btn text-right">
                              <a class="btn btn-theme-colored btn-xs" href="shop-cart.html"> View cart</a>
                              <a class="btn btn-dark btn-xs" href="shop-checkout.html"> Checkout</a>
                          </div>
                        </div>
                        <!-- dropdown cart ends -->
                      </li>
                    </ul>
                  </div>
                </li>--}}
                <li class="mb-0 pb-0">
                  <div class="top-dropdown-outer pt-5 pb-10">
                    <a class="top-search-box has-dropdown text-white text-hover-theme-colored"><i class="fa fa-search font-13"></i> &nbsp;</a>
                    <ul class="dropdown">
                      <li>
                        <div class="search-form-wrapper">
                          <form method="get" class="mt-10">
                            <input type="text" onfocus="if(this.value =='Enter your search') { this.value = ''; }" onblur="if(this.value == '') { this.value ='Enter your search'; }" value="Enter your search" id="searchinput" name="s" class="">
                            <label><input type="submit" name="submit" value=""></label>
                          </form>
                        </div>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
            <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">
              <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-middle p-0 bg-lightest xs-text-center">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-5">
            <div class="widget no-border m-0">
              <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="javascript:void(0)"><img src="{{asset('images/'.$banner->logo)}}" alt=""></a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-phone-square text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-gray text-uppercase">KONTAK</a>
                  <h5 class="font-14 m-0"> {{App\Banner::find(1)->telpon}}</h5>
                </li>
              </ul>
            </div>
          </div>  
          <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">
              <ul class="list-inline">
                <li><i class="fa fa-clock-o text-theme-colored font-36 mt-5 sm-display-block"></i></li>
                <li>
                  <a href="#" class="font-12 text-gray text-uppercase">We are open!</a>
                  <h5 class="font-13 text-black m-0"> Mon-Fri 8:00-16:00</h5>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-colored border-bottom-theme-color-2-1px">
        <div class="container">
          <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
            <ul class="menuzord-menu">
              <li class="{{setActive(['index'])}}"><a href="{{route('index')}}">Beranda</a></li>
              <!-- <li><a href="#about">About</a></li> -->
              <li class="{{setActive(['sejarah_sekolah','visi_misi','struktur_organisasi','kultur_sekolah','layanan_pendidikan'])}}"><a href="#">Profil Sekolah</a>
                <ul class="dropdown">
                  @foreach(App\MenuDepan::where('category','profil')->where('is_active','yes')->get() as $keys => $profil)
                  <li><a href="{{route($profil->url)}}">{{$profil->menu}}</a></li>
                  @endforeach
                  {{--<li><a href="{{route('sejarah_sekolah')}}"> Sejarah</a></li>
                  <li><a href="{{route('visi_misi')}}">Visi Misi</a></li>
                  <li><a href="{{route('struktur_organisasi')}}">Struktur Organisasi</a></li>
                  <li><a href="{{route('kultur_sekolah')}}">Kultur Sekolah</a></li>
                  <li><a href="{{route('layanan_pendidikan')}}">Layanan Pendidikan</a></li> --}}
                </ul>
              </li>
              <li class="{{setActive(['ruang_kelas','ruang_perpustakaan','ruang_laboraturiumn','aula_sekolah','mesjid','front_office','kantin_sekolah','taman_sekolah','toilet','bus_sekolah'])}}"><a href="#">Fasilitas</a>
                <ul class="dropdown">
                  @foreach(App\MenuDepan::where('category','fasilitas')->where('is_active','yes')->get() as $keys => $fasilitas)
                  <li><a href="{{route($fasilitas->url)}}">{{$fasilitas->menu}}</a></li>
                  @endforeach
                  {{-- <li><a href="{{route('ruang_kelas')}}">Ruang Kelas</a></li>
                  <li><a href="{{route('ruang_perpustakaan')}}">Ruang Perpustakaan</a></li>
                  <li><a href="{{route('ruang_laboraturiumn')}}">Laboraturium</a></li>
                  <li><a href="{{route('aula_sekolah')}}">Aula Sekolah</a></li>
                  <li><a href="{{route('mesjid')}}">Mesjid</a></li>
                  <li><a href="{{route('front_office')}}">Front Office</a></li>
                  <li><a href="{{route('kantin_sekolah')}}">Kantin Sekolah</a></li>
                  <li><a href="{{route('taman_sekolah')}}">Taman Sekolah</a></li>
                  <li><a href="{{route('toilet')}}">Toilet</a></li>
                  <li><a href="{{route('bus_sekolah')}}">Bus Sekolah</a></li> --}}
                </ul>
              </li>
              <li class="{{setActive(['e_kurikulum','e_penilaian','e_rapor','dapodik','si_lulus','e_learning','kursus'])}}"><a href="#">Akademik</a>
                <ul class="dropdown">
                  <li><a href="{{route('e_kurikulum')}}">E-Kurikulum</a></li>
                  <li><a href="{{route('e_penilaian')}}">E-Penilaian</a></li>
                  <li><a href="{{route('e_rapor')}}">E-Rapor</a></li>
                  <li><a href="{{route('dapodik')}}">Dapodik</a></li>
                  <li><a href="{{route('si_lulus')}}">Si-Lulus</a></li>
                  <li><a href="{{route('e_learning')}}">E-Learning</a></li>
                  <li><a href="{{route('kursus')}}">Kursus</a></li>
                </ul>
              </li>
              <li class="{{setActive(['guru-guru','karyawan','detail-guru'])}}"><a href="#">Guru & Karyawan</a>
                <ul class="dropdown">
                  <li ><a href="{{route('guru-guru')}}">Guru</a></li>
                  <li><a href="{{route('karyawan')}}">Karyawan</a></li>
                </ul>
              </li>
              <li class="{{setActive(['gallery'])}}"><a href="{{route('gallery')}}">Galleri</a></li>
              <li class="{{setActive(['blog','detail_blog'])}}"><a href="{{route('blog')}}">Berita</a></li>
              <li class="{{setActive(['kontak'])}}"><a href="{{route('kontak')}}">Kontak</a></li>
                @if(Auth::guard())
                @if(Auth::guard('user')->check())
                <li class="{{setActive(['siswa','siswa-alur-pendaftaran'])}}"><a href="#">Akun</a>
                  <ul class="dropdown">
                  <li><a href="{{url('siswa')}}">Data Pendaftaran Saya</a></li>
                  <li><a href="{{route('siswa-alur-pendaftaran')}}">Alur Pendaftaran</a></li>
                  <li><a href="{{url('/logout/user')}}">Keluar</a></li>
                  </ul>
                </li>
                @elseif(Auth::guard('petugas')->check())
                <li><a href="#">Akun</a>
                  <ul class="dropdown">
                  <li><a href="{{route('petugas')}}">Halaman Petugas</a></li>
                  <li><a href="{{url('/logout/petugas')}}">Keluar</a></li>
                  </ul>
                </li>
                @elseif(Auth::guard('guru')->check())
                <li><a href="#">Akun</a>
                  <ul class="dropdown">
                  <li><a href="{{route('guru')}}">Halaman Guru</a></li>
                  <li><a href="{{url('/logout/guru')}}">Keluar</a></li>
                  </ul>
                </li>
                @elseif(Auth::guard('admin')->check())
                <li><a href="#">Akun</a>
                  <ul class="dropdown">
                  <li><a href="{{url('/admin')}}">Halaman Admin</a></li>
                  <li><a href="{{url('/logout/admin')}}">Keluar</a></li>
                  </ul>
                </li>
                @endif
                @endif
                
            </ul>
            <ul class="pull-right flip hidden-sm hidden-xs">
              <li>
                <!-- Modal: Book Now Starts -->
                <a class="btn btn-colored btn-flat bg-theme-color-2 text-white font-14 bs-modal-ajax-load mt-0 p-25 pr-15 pl-15"  href="{{route('daftar')}}">Pendaftaran Sekolah</a>
                <!-- Modal: Book Now End -->
              </li>
            </ul>
            <div id="top-search-bar" class="collapse">
              <div class="container">
                <form role="search" action="#" class="search_form_top" method="get">
                  <input type="text" placeholder="Type text and press Enter..." name="s" class="form-control" autocomplete="off">
                  <span class="search-close"><i class="fa fa-search"></i></span>
                </form>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>