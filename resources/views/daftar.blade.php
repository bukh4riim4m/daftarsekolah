@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<div class="main-content">

<!-- Section: inner-header -->
<section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
  <div class="container pt-70 pb-20">
    <!-- Section Content -->
    <div class="section-content">
      <div class="row">
        <div class="col-md-12">
          <h2 class="title text-white">Pendaftaran Siswa Baru</h2>
          <ol class="breadcrumb text-left text-black mt-10">
            <li><a href="{{url('/')}}">Beranda</a></li>
            <li class="active text-gray-silver">Pendaftaran Sekolah</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Section: Job Apply Form -->
<section class="divider">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-md-push-0">
      @include('flash::message')
        <div class="border-1px p-30 mb-0">

          <form id="job_apply_form"  action="{{route('daftar')}}" method="post" id="proses-daftar" enctype="multipart/form-data">
          @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-line-bottom">
                        <h4 class="heading-title">IDENTITAS CALON SISWA</h4>
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">1. Nama Lengkap</label>
                    <input type="text" class="form-control @error('name') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="name" value="{{old('name')}}">
                    @error('name')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">2. Nama Panggilan</label>
                    <input type="text" class="form-control @error('nama_panggilan') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="nama_panggilan" value="{{old('nama_panggilan')}}">
                    @error('nama_panggilan')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">3. Jenis Kelamin</label>
                    <select name="jenis_kelamin_id" class="form-control">
                        <?php $jenkel = App\JenisKelamin::get();?>
                        @foreach($jenkel as $jk)
                            @if($jk->id == old('jenis_kelamin_id'))
                            <option value="{{$jk->id}}" selected>{{$jk->jenis_kelamin}}</option>
                            @else
                            <option value="{{$jk->id}}">{{$jk->jenis_kelamin}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">4. Kota/Kab.Tempat Kelahiran</label>
                    <input type="text" class="form-control @error('tempat_lahir') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="tempat_lahir" value="{{old('tempat_lahir')}}">
                    @error('tempat_lahir')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">5. Tgl/Bln/Thn Kelahiran</label>
                    <div class="row">
                        <div class="col-md-4">
                            <select name="tgl_lahir" id=""  class="form-control @error('tgl_lahir') error @enderror">
                            <option value="">Tanggal</option>
                                <?php  for ($i=1; $i <= 31 ; $i++) { 
                                    if ($i == old('tgl_lahir')) {
                                        echo "<option value='".$i."' selected>".$i."</option>";
                                    }else {
                                        echo "<option value='".$i."'>".$i."</option>";
                                    }
                                    
                                }?>
                            </select>
                            @error('tgl_lahir')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <select name="bulan_lahir" id=""  class="form-control @error('bulan_lahir') error @enderror">
                                <option value="">Bulan</option>
                                    <?php  for ($i=1; $i <= 12 ; $i++) { 
                                        if ($i == old('bulan_lahir')) {
                                            echo "<option value='".$i."' selected>".$i."</option>";
                                        }else {
                                            echo "<option value='".$i."'>".$i."</option>";
                                        }
                                    }?>
                            </select>
                            @error('bulan_lahir')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-4">
                        <input type="number" name="tahun_lahir" placeholder="Tahun" class="form-control @error('tahun_lahir') error @enderror" value="{{old('tahun_lahir')}}"> 
                            @error('tahun_lahir')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    
                </div>
              </div>
              
              
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">6. Nomor Telepon/HP</label>
                    <input type="number" class="form-control @error('hp') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="hp" value="{{old('hp')}}">
                    @error('hp')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-form-label">7. Alamat Lengkap</label>
                    <textarea class="form-control @error('alamat_lengkap') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="alamat_lengkap" id="" cols="30" rows="3">{{old('alamat_lengkap')}}</textarea>
                    @error('alamat_lengkap')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">8. Nama Sekalah Asal</label>
                    <input type="text" class="form-control @error('sekolah_asal') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="sekolah_asal" value="{{old('sekolah_asal')}}">
                    @error('sekolah_asal')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">9. Alamat Sekolah Asal</label>
                    <input type="text" class="form-control @error('alamat_sekolah_asal') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="alamat_sekolah_asal" value="{{old('alamat_sekolah_asal')}}">
                    @error('alamat_sekolah_asal')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            
            
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">10. Jumlah Saudara Kandung</label>
                    <input type="number" class="form-control @error('jumlah_saudara_kandung') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="jumlah_saudara_kandung" value="{{old('jumlah_saudara_kandung')}}">
                    @error('jumlah_saudara_kandung')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">11. Jumlah Saudara Tiri</label>
                    <input type="number" class="form-control @error('jumlah_saudara_tiri') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="jumlah_saudara_tiri" value="{{old('jumlah_saudara_tiri')}}">
                    @error('jumlah_saudara_tiri')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">12. Jumlah Saudara Angkat</label>
                    <input type="number" class="form-control @error('jumlah_saudara_angkat') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="jumlah_saudara_angkat" value="{{old('jumlah_saudara_angkat')}}">
                    @error('jumlah_saudara_angkat')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">13. Anak Yatim/Piyatu/Yatim Piyatu</label>
                    <select name="yatim_piyatu_id" id="" class="form-control @error('yatim_piyatu_id') error @enderror">
                    <?php $yatims = App\YatimPiyatu::get();?>
                        @foreach($yatims as $yatim)
                            @if($yatim->id == old('yatim_piyatu_id'))
                            <option value="{{$yatim->id}}" selected>{{$yatim->name}}</option>
                            @else
                            <option value="{{$yatim->id}}">{{$yatim->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('yatim_piyatu_id')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">14. Bahasa Sehari Hari</label>
                    <input type="text" class="form-control @error('bahasa_sehari_hari') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="bahasa_sehari_hari" value="{{old('bahasa_sehari_hari')}}">
                    @error('bahasa_sehari_hari')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">15. Bangsa Negara</label>
                    <input type="text" class="form-control @error('bangsa_negara') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="bangsa_negara" value="{{old('bangsa_negara')}}">
                    @error('bangsa_negara')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">16. Kelainan Jasmani</label>
                    <input type="text" class="form-control @error('kelainan_jasmani') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="kelainan_jasmani" value="{{old('kelainan_jasmani')}}">
                    @error('kelainan_jasmani')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">17. Punya Penyakit Berat</label>
                    <input type="text" class="form-control @error('penyakit') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="penyakit" value="{{old('penyakit')}}">
                    @error('penyakit')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">18. Tinggi & Berat Badan</label>
                    <div class="row">
                        <div class="col-md-6">
                        <input type="number" class="form-control @error('tinggi_badan') error @enderror" placeholder="Tinggi Badan" name="tinggi_badan" value="{{old('tinggi_badan')}}">
                        @error('tinggi_badan')
                        <p class="error">{{ $message }}</p>
                        @enderror
                        </div>
                        <div class="col-md-6">
                        <input type="number" class="form-control @error('berat_badan') error @enderror" placeholder="Berat Badan" name="berat_badan" value="{{old('berat_badan')}}">
                        @error('berat_badan')
                        <p class="error">{{ $message }}</p>
                        @enderror
                        </div>
                    </div>
                    
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">19. Prestasi Yang Pernah diraih</label>
                    <input type="text" class="form-control @error('prestasi') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="prestasi" value="{{old('prestasi')}}">
                    @error('prestasi')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-line-bottom">
                        <h4 class="heading-title">IDENTITAS ORANG TUA (AYAH)</h4>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">1. Nama Lengkap</label>
                    <input type="text" class="form-control @error('nama_ayah') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="nama_ayah" value="{{old('nama_ayah')}}">
                    @error('nama_ayah')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">2. Tempat Lahir</label>
                    <input type="text" class="form-control @error('tempat_lahir_ayah') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="tempat_lahir_ayah" value="{{old('tempat_lahir_ayah')}}">
                    @error('tempat_lahir_ayah')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">3. Tanggal Lahir</label>
                    <div class="row">
                        <div class="col-md-4">
                            <select name="tgl_lahir_ayah" id=""  class="form-control @error('tgl_lahir_ayah') error @enderror">
                            <option value="">Tanggal</option>
                                <?php  for ($i=1; $i <= 31 ; $i++) { 
                                    if ($i == old('tgl_lahir_ayah')) {
                                        echo "<option value='".$i."' selected>".$i."</option>";
                                    }else {
                                        echo "<option value='".$i."'>".$i."</option>";
                                    }
                                    
                                }?>
                            </select>
                            @error('tgl_lahir_ayah')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <select name="bulan_lahir_ayah" id=""  class="form-control @error('bulan_lahir_ayah') error @enderror">
                                <option value="">Bulan</option>
                                    <?php  for ($i=1; $i <= 12 ; $i++) { 
                                        if ($i == old('bulan_lahir_ayah')) {
                                            echo "<option value='".$i."' selected>".$i."</option>";
                                        }else {
                                            echo "<option value='".$i."'>".$i."</option>";
                                        }
                                    }?>
                            </select>
                            @error('bulan_lahir_ayah')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-4">
                        <input type="number" name="tahun_lahir_ayah" placeholder="Tahun" class="form-control @error('tahun_lahir_ayah') error @enderror" value="{{old('tahun_lahir_ayah')}}"> 
                            @error('tahun_lahir_ayah')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">4. Agama</label>
                    <?php $agamas = App\Agama::get();?>
                    <select name="agama_ayah_id" id="" class="form-control @error('agama_ayah_id') error @enderror">
                        @foreach($agamas as $agama)
                            @if($agama->id == old('agama_ayah_id'))
                            <option value="{{$agama->id}}" selected>{{$agama->agama}}</option>
                            @else
                            <option value="{{$agama->id}}">{{$agama->agama}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('agama_ayah_id')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">5. Pendidikan</label>
                    <?php $pendidkan_ayahs = App\Pendidikan::get();?>
                    <select name="pendiddikan_ayah_id" id="" class="form-control @error('pendiddikan_ayah_id') error @enderror">
                        @foreach($pendidkan_ayahs as $pa)
                            @if($pa->id == old('pendiddikan_ayah_id'))
                            <option value="{{$pa->id}}" selected>{{$pa->pendidikan}}</option>
                            @else
                            <option value="{{$pa->id}}">{{$pa->pendidikan}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('pendiddikan_ayah_id')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">6. Pekerjaan / Instansi</label>
                    <input type="text" class="form-control @error('pekerjaan_ayah') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="pekerjaan_ayah" value="{{old('pekerjaan_ayah')}}">
                    @error('pekerjaan_ayah')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">7. Warga Negara</label>
                    <input type="text" class="form-control @error('warga_negara_ayah') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="warga_negara_ayah" value="{{old('warga_negara_ayah')}}">
                    @error('warga_negara_ayah')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">8. Nomor Telpon/HP</label>
                    <input type="number" class="form-control @error('nomor_telpon_ayah') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="nomor_telpon_ayah" value="{{old('nomor_telpon_ayah')}}">
                    @error('nomor_telpon_ayah')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">9. Pendapatan Perbulan</label>
                    <?php $pendapatan_ayahs = App\Pendapatan::get();?>
                    <select name="pedapatan_ayah" id="" class="form-control @error('pedapatan_ayah') error @enderror">
                        @foreach($pendapatan_ayahs as $penay)
                            @if($penay->id == old('pedapatan_ayah'))
                            <option value="{{$penay->id}}" selected>{{$penay->pendapatan}}</option>
                            @else
                            <option value="{{$penay->id}}">{{$penay->pendapatan}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('pedapatan_ayah')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
              

            <div class="row">
                <div class="col-md-12">
                    <div class="heading-line-bottom">
                        <h4 class="heading-title">IDENTITAS ORANG TUA (IBU)</h4>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">1. Nama Lengkap</label>
                    <input type="text" class="form-control @error('nama_ibu') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="nama_ibu" value="{{old('nama_ibu')}}">
                    @error('nama_ibu')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">2. Tempat Lahir</label>
                    <input type="text" class="form-control @error('tempat_lahir_ibu') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="tempat_lahir_ibu" value="{{old('tempat_lahir_ibu')}}">
                    @error('tempat_lahir_ibu')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">3. Tanggal Lahir</label>
                    <div class="row">
                    <div class="col-md-4">
                            <select name="tgl_lahir_ibu" id=""  class="form-control @error('tgl_lahir_ibu') error @enderror">
                            <option value="">Tanggal</option>
                                <?php  for ($i=1; $i <= 31 ; $i++) { 
                                    if ($i == old('tgl_lahir_ibu')) {
                                        echo "<option value='".$i."' selected>".$i."</option>";
                                    }else {
                                        echo "<option value='".$i."'>".$i."</option>";
                                    }
                                    
                                }?>
                            </select>
                            @error('tgl_lahir_ibu')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <select name="bulan_lahir_ibu"  class="form-control @error('bulan_lahir_ibu') error @enderror">
                                <option value="">Bulan</option>
                                    <?php  for ($i=1; $i <= 12 ; $i++) { 
                                        if ($i == old('bulan_lahir_ibu')) {
                                            echo "<option value='".$i."' selected>".$i."</option>";
                                        }else {
                                            echo "<option value='".$i."'>".$i."</option>";
                                        }
                                    }?>
                            </select>
                            @error('bulan_lahir_ibu')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-4">
                        <input type="number" name="tahun_lahir_ibu" placeholder="Tahun" class="form-control @error('tahun_lahir_ibu') error @enderror" value="{{old('tahun_lahir_ibu')}}"> 
                            @error('tahun_lahir_ibu')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">4. Agama</label>
                    <?php $agamasi = App\Agama::get();?>
                    <select name="agama_ibu_id" id="" class="form-control @error('agama_ibu_id') error @enderror">
                        @foreach($agamasi as $agamasii)
                            @if($agama->id == old('agama_ibu_id'))
                            <option value="{{$agamasii->id}}" selected>{{$agamasii->agama}}</option>
                            @else
                            <option value="{{$agamasii->id}}">{{$agamasii->agama}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('agama_ibu_id')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">5. Pendidikan</label>
                    <?php $pendidkan_ibus = App\Pendidikan::get();?>
                    <select name="pendiddikan_ibu_id" id="" class="form-control @error('pendiddikan_ibu_id') error @enderror">
                        @foreach($pendidkan_ibus as $pi)
                            @if($pi->id == old('pendiddikan_ibu_id'))
                            <option value="{{$pi->id}}" selected>{{$pi->pendidikan}}</option>
                            @else
                            <option value="{{$pi->id}}">{{$pi->pendidikan}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('pendiddikan_ibu_id')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">6. Pekerjaan / Instansi</label>
                    <input type="text" class="form-control @error('pekerjaan_ibu') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="pekerjaan_ibu" value="{{old('pekerjaan_ibu')}}">
                    @error('pekerjaan_ibu')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">7. Warga Negara</label>
                    <input type="text" class="form-control @error('warga_negara_ibu') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="warga_negara_ibu" value="{{old('warga_negara_ibu')}}">
                    @error('warga_negara_ibu')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">8. Nomor Telpon/HP</label>
                    <input type="number" class="form-control @error('nomor_telpon_ibu') error @enderror" placeholder=". . . . . . . . . . . . . . . . . . ." name="nomor_telpon_ibu" value="{{old('nomor_telpon_ibu')}}">
                    @error('nomor_telpon_ibu')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">9. Pendapatan Perbulan</label>
                    <?php $pendapatan_ibus = App\Pendapatan::get();?>
                    <select name="pedapatan_ibu" id="" class="form-control @error('pedapatan_ibu') error @enderror">
                        @foreach($pendapatan_ibus as $peni)
                            @if($peni->id == old('pedapatan_ibu'))
                            <option value="{{$peni->id}}" selected>{{$peni->pendapatan}}</option>
                            @else
                            <option value="{{$peni->id}}">{{$peni->pendapatan}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('pedapatan_ibu')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
            </div>
              
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-line-bottom">
                        <h4 class="heading-title">KATA SANDI UNTUK LOGIN</h4>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">Kata Sandi</label>
                    <input type="text" class="form-control @error('password') error @enderror" placeholder="******" name="password" value="{{old('password')}}">
                    @error('password')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="col-form-label">Ulangi Kata Sandi</label>
                    <input type="text" class="form-control @error('password_confirmation') error @enderror" placeholder="******" name="password_confirmation" value="{{old('password_confirmation')}}">
                    @error('password_confirmation')
                    <p class="error">{{ $message }}</p>
                    @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group"><center>
                    <!-- <label class="col-form-label">Upload Foto</label> -->
                    <input type="file" id="pic" name="foto" style="display:none" onchange="document.getElementById('filename').value=this.value">
                    
                    <img src="{{asset('images/canvas.png')}}" alt="" onclick="document.getElementById('pic').click()">
                    <br><input type="text" id="filename" disabled style="width:110px;" class="@error('foto') error @enderror"><br>
                    @error('foto')
                    <p class="error">{{ $message }}</p>
                    @enderror</center>
                    <!-- <input type="button" value="Pilih Foto" onclick="document.getElementById('pic').click()"> -->
                </div>
              </div>
            </div>
              
            </div>

              <div class="form-group">
              <input name="form_botcheck" class="form-control" type="hidden" value="" />
              <button type="submit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" data-loading-text="Please wait...">KIRIM PENDAFTARAN</button>
            </div>
          </form>

          <!-- Job Form Validation-->
          <script type="text/javascript">
            $("#job_apply_form").validate({
              submitHandler: function(form) {
                var form_btn = $(form).find('button[type="submit"]');
                var form_result_div = '#form-result';
                $(form_result_div).remove();
                $("#preloader").fadeIn('slow');
                document.getElementById('proses-daftar').submit();"
                form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                var form_btn_old_msg = form_btn.html();
                form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                $(form).ajaxSubmit({
                  dataType:  'json',
                  success: function(data) {
                    if( data.status == 'true' ) {
                      $(form).find('.form-control').val('');
                    }
                    form_btn.prop('disabled', false).html(form_btn_old_msg);
                    $(form_result_div).html(data.message).fadeIn('slow');
                    setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                  }
                });
              }
            });
          </script>
        </div>
      </div>
    </div>
  </div>
</section> 
</div>  
@endsection
@section('js')
@endsection