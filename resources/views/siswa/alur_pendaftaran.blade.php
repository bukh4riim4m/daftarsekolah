@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Alur Pendaftaran</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Alur Pendaftaran</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div>
        <div class="row">
        @include('flash::message')
          <div class="col-md-12 text-center">
            <div class="heading-line-bottom text-center mb-60">
              <h4>Alur Proses Calon Siswa</h4>
            </div>
          </div>
        </div>
        <!-- <div class="row">
          <ul class="working-process">
            <li class="col-xs-12 col-sm-2">
              <a href="#"><i class="fa pe-7s-monitor"></i></a>
              <h5>1. Pendaftaran</h5>
            </li>
            <li class="col-xs-12 col-sm-2">
              <a href="#"><i class="fa pe-7s-users"></i></a>
              <h5>2. Verifikasi Berkas</h5>
            </li>
            <li class="col-xs-12 col-sm-2">
              <a href="#"><i class="fa fa-paint-brush"></i></a>
              <h5>Design</h5>
            </li>
            <li class="col-xs-12 col-sm-2">
              <a href="#"><i class="fa fa-wrench"></i></a>
              <h5>Build</h5>
            </li>
            <li class="col-xs-12 col-sm-2">
              <a href="#"><i class="fa fa-rocket"></i></a>
              <h5>Launch</h5>
            </li>
            <li class="col-xs-12 col-sm-2">
              <a href="#"><i class="fa fa-wifi"></i></a>
              <h5>Promote</h5>
            </li>
          </ul>
        </div> -->
        
      </div>
    </section>
    <section id="cd-timeline" class="cd-container">
        @foreach($datas as $key => $data)
      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-picture">
          <img src="{{asset('images/flat-color-icons-svg/portrait_mode.svg')}}" alt="Picture">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
          <h2>{{$key+1}}. {{$data->judul}}</h2>
          <p>{{$data->keterangan}}</p>
          <!-- <a href="#0" class="cd-read-more">Read more</a> -->
          <span class="cd-date">{{date('d M Y', strtotime($data->tgl_mulai))}} s/d {{date('d M Y', strtotime($data->tgl_selesai))}}</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->
        @endforeach
      {{--<div class="cd-timeline-block">
        <div class="cd-timeline-img cd-movie">
          <img src="js/vertical-timeline/img/cd-icon-movie.svg" alt="Movie">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
          <h2>Title of section 2</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde?</p>
          <a href="#0" class="cd-read-more">Read more</a>
          <span class="cd-date">Jan 18</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-picture">
          <img src="js/vertical-timeline/img/cd-icon-picture.svg" alt="Picture">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
          <h2>Title of section 3</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, obcaecati, quisquam id molestias eaque asperiores voluptatibus cupiditate error assumenda delectus odit similique earum voluptatem doloremque dolorem ipsam quae rerum quis. Odit, itaque, deserunt corporis vero ipsum nisi eius odio natus ullam provident pariatur temporibus quia eos repellat consequuntur perferendis enim amet quae quasi repudiandae sed quod veniam dolore possimus rem voluptatum eveniet eligendi quis fugiat aliquam sunt similique aut adipisci.</p>
          <a href="#0" class="cd-read-more">Read more</a>
          <span class="cd-date">Jan 24</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-location">
          <img src="js/vertical-timeline/img/cd-icon-location.svg" alt="Location">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
          <h2>Title of section 4</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
          <a href="#0" class="cd-read-more">Read more</a>
          <span class="cd-date">Feb 14</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-location">
          <img src="js/vertical-timeline/img/cd-icon-location.svg" alt="Location">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
          <h2>Title of section 5</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>
          <a href="#0" class="cd-read-more">Read more</a>
          <span class="cd-date">Feb 18</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-movie">
          <img src="js/vertical-timeline/img/cd-icon-movie.svg" alt="Movie">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
          <h2>Final Section</h2>
          <p>This is the content of the last section</p>
          <span class="cd-date">Feb 26</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->--}}
    </section>
@endsection
@section('js')
@endsection