@extends('layouts.app')
@section('css')
@endsection
@section('content')  
<section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Data Pendaftaran</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Data Pendaftaran</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
<section>
      <div class="container">
        <div class="row">
        @include('flash::message')
          <div class="col-md-6 mt-40">
            <h4 class="title">Data Calon Siswa</h4>
            <p>Data yang diisi saat pendaftaran online.</p>
            <div class="table-responsive"> 
              <table class="table table-bordered table-striped"> 
                <colgroup> 
                  <col class="col-xs-6"> 
                  <col class="col-xs-6"> 
                </colgroup> 
                <thead> 
                  <tr> 
                    <th>Title</th> 
                    <th>Description</th> 
                  </tr> 
                </thead> 
                <tbody> 

                  <tr> 
                    <th scope="row"> 
                      <code>Foto</code> 
                    </th> 
                    <td><img src="{{asset('images/siswa/'.Auth::user()->foto)}}" alt=""></td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Nomor Pendaftaran</code> 
                    </th> 
                    <td>{{Auth::user()->nomor_pendaftaran}}</td> 
          </tr>
          <tr> 
                    <th scope="row"> 
                      <code>Nama Lengkap</code> 
                    </th> 
                    <td>{{Auth::user()->name}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Nama Panggilan</code> 
                    </th> 
                    <td>{{Auth::user()->nama_panggilan}}</td> 
				  </tr> 
				  <tr> 
                    <th scope="row"> 
                      <code>Jenis Kelamin</code> 
                    </th> 
                    <td>{{Auth::user()->jenisKelaminSiswa->jenis_kelamin}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Tempat Lahir</code> 
                    </th> 
                    <td>{{Auth::user()->tempat_lahir}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Tanggal Lahir</code> 
                    </th> 
                    <td>{{Auth::user()->tgl_lahir}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Nomor Telepon/HP</code> 
                    </th> 
                    <td>{{Auth::user()->hp}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Email</code> 
                    </th> 
                    <td>{{Auth::user()->email}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Alamat Lengkap</code> 
                    </th> 
                    <td>{{Auth::user()->alamat_lengkap}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Nama Sekalah Asal</code> 
                    </th> 
                    <td>{{Auth::user()->sekolah_asal}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Alamat Sekolah Asal</code> 
                    </th> 
                    <td>{{Auth::user()->alamat_sekolah_asal}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Jumlah Saudara Kandung</code> 
                    </th> 
                    <td>{{Auth::user()->jumlah_saudara_kandung}} Orang</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Jumlah Saudara Tiri</code> 
                    </th> 
                    <td>{{Auth::user()->jumlah_saudara_tiri}} Orang</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Jumlah Saudara Angkat</code> 
                    </th> 
                    <td>{{Auth::user()->jumlah_saudara_angkat}} Orang</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Anak Yatim/Piyatu/Yatim Piyatu</code> 
                    </th> 
                    <td>{{Auth::user()->anakYatimPiyatu->name}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Bahasa Sehari Hari</code> 
                    </th> 
                    <td>{{Auth::user()->bahasa_sehari_hari}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Bangsa Negara</code> 
                    </th> 
                    <td>{{Auth::user()->bangsa_negara}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Kelainan Jasmani</code> 
                    </th> 
                    <td>{{Auth::user()->kelainan_jasmani}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Punya Penyakit Berat</code> 
                    </th> 
                    <td>{{Auth::user()->penyakit}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Tinggi & Berat Badan</code> 
                    </th> 
                    <td>{{Auth::user()->tinggi_badan}} Kg & {{Auth::user()->berat_badan}} Cm</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Prestasi Yang Pernah diraih</code> 
                    </th> 
                    <td>{{Auth::user()->prestasi}}</td> 
				  </tr>
				  <tr> 
                    <td> <a href="{{route('siswa_edit_data')}}"> <button class="btn btn-success form-control"><i class="fa fa-edit"></i> EDIT</button></a></td>
                    <td><button class="btn btn-primary form-control" onclick="event.preventDefault();
                        document.getElementById('download').submit();"><i class="fa fa-download"></i> DOWNLOAD BERKAS</button></td> 
                    <form action="{{route('siswa_download_berkas')}}" method="post" id="download">
                      @csrf
                    </form>
				  </tr>
                 </tbody> 
                </table> 
            </div>
		  </div>
		  <div class="col-md-6 mt-40">
            <h4 class="title">Data Orang Tua Calon Siswa</h4>
            <p>Data yang diisi saat pendaftaran online.</p>
            <div class="table-responsive"> 
              <table class="table table-bordered table-striped"> 
                <colgroup> 
                  <col class="col-xs-6"> 
                  <col class="col-xs-6"> 
                </colgroup> 
                <thead> 
                  <tr> 
                    <th>Title</th> 
                    <th>Description</th> 
                  </tr> 
                </thead> 
                <tbody> 

                  <tr> 
                    <th scope="row"> 
                      <code>Nama Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->nama_ayah}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Tempat Lahir Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->tempat_lahir_ayah}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Tanggal Lahir Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->tgl_lahir_ayah}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Nomor Telepon/HP Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->nomor_telpon_ayah}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Agama Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->agamaAyah->agama}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Pendidikan Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->pendidikanAyah->pendidikan}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Pekerjaan Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->pekerjaan_ayah}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Pendapatan Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->pendapatanAyah->pendapatan}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Warga Negara Ayah</code> 
                    </th> 
                    <td>{{Auth::user()->warga_negara_ayah}}</td> 
				  </tr>
				  <tr> 
				  	<td> <hr></td> 
                    <td> <hr></td> 
				  </tr>

				  <tr> 
                    <th scope="row"> 
                      <code>Nama Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->nama_ibu}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Tempat Lahir Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->tempat_lahir_ibu}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Tanggal Lahir Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->tgl_lahir_ibu}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Nomor Telepon/HP Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->nomor_telpon_ibu}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Agama Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->agamaIbu->agama}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Pendidikan Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->pendidikanIbu->pendidikan}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Pekerjaan Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->pekerjaan_ibu}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Pendapatan Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->pendapatanIbu->pendapatan}}</td> 
				  </tr>
				  <tr> 
                    <th scope="row"> 
                      <code>Warga Negara Ibu</code> 
                    </th> 
                    <td>{{Auth::user()->warga_negara_ibu}}</td> 
				  </tr>
                 </tbody> 
                </table> 
            </div>
          </div>
        </div>
      </div>
    </section> 
@endsection
@section('js')
@endsection