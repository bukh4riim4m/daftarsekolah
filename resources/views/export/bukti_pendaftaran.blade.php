<!DOCTYPE html>
<html lang="en">
<head>

    <title>Document</title>
    <style>
            /** Define the margins of your page **/
            @page {
                margin: 25px 25px;
            }
            #foto {
                position: fixed; 
                bottom:     150px; 
                text-align: center;
            }
            #diisi{
                text-align: right;
                position: sticky; 
                margin-right:0px;
                margin-left:auto;
            }
            #wali{
                text-align: left;
                position: fixed;
                bottom:     150px;
                left:0px; 
                width:33%;
            }
            #kepsek{
                text-align: right;
                position: fixed;
                bottom:     150px;
                right:0px; 
                width:33%;
            }
        </style>
</head>
<body>

    <img src="{{public_path() . '/images/sliders/logo_sma.png'}}" id="logo" style="width:95px; position: absolute;"/>
<center><font style="font-size:25px;text-align:center;">DINAS PENDIDIKAN PROPINSI <br>NUSA TENGGARA BARAT</font><font><br>Jl. Pelabuhan Sape, <br>Rasabou, Bima, Nusa Tenggara Barat. 84182</font>
<hr><br>
<font style="text-align:center;font-size:22px;"><b>BUKTI PENDAFTARAN</b></font><br>
<font style="text-align:center;font-size:20px;"><u>SMU NEGERI 1 SAPE 2020</u></font></center> 
<br>
    <table class="table" width="100%" style="border:1px solid black;border-collapse:collapse;">
        <tbody>
            <tr>
                <td style="padding:7px;border:1px solid black;" width="40%">Nomor Pendaftaran</td>
                <td style="padding:7px;border:1px solid black;">{{$users->nomor_pendaftaran}}</td>
            </tr>        
            <tr>
                <td style="padding:7px;border:1px solid black;" width="40%">Tanggal Pendaftaran</td>
                <td style="padding:7px;border:1px solid black;">{{date('d M Y', strtotime($users->created_at))}}</td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <table class="table" width="100%" style="border:1px solid black;border-collapse:collapse;">
        <tbody>
            
            <tr>
                <td style="padding:7px;border:1px solid black;" width="40%">Nama Lengkap</td>
                <td style="padding:7px;border:1px solid black;">{{$users->name}}</td>
            </tr>
            <tr>
                <td style="padding:7px;border:1px solid black;" width="40%">Jenis Kelamin</td>
                <td style="padding:7px;border:1px solid black;">Laki-Laki</td>
            </tr>
            <tr>
                <td style="padding:7px;border:1px solid black;" width="40%">Tempat & Tanggal Lahir</td>
                <td style="padding:7px;border:1px solid black;">{{$users->tempat_lahir}},{{$users->tgl_lahir}}</td>
            </tr>
            <tr>
                <td style="padding:7px;border:1px solid black;" width="40%">Nomor Telepon/HP</td>
                <td style="padding:7px;border:1px solid black;">{{$users->hp}}</td>
            </tr>
            <tr>
                <td style="padding:7px;border:1px solid black;" width="40%">Alamat Lengkap</td>
                <td style="padding:7px;border:1px solid black;">{{$users->alamat_lengkap}}</td>
            </tr>
        </tbody>
    </table><br>
    <table width="100%">
        <tr>
            <td>Syarat-syarat yang dibawa saat Verifikasi Berkas :</td><td style="text-align:right;">*Diisi Petugas*</td>
        </tr>
    </table>
    <table class="table" width="100%" style="border:1px solid black;border-collapse:collapse;">
        <thead>
            <tr>
                <th style="padding:7px;border:1px solid black;width:30px;">No</th>
                <th style="padding:7px;border:1px solid black;text-align:left;">Berkas</th>
                <th style="padding:7px;border:1px solid black;width:80px;text-align:center;">Ada</th>
                <th style="padding:7px;border:1px solid black;width:80px;text-align:center;">Tidak Ada</th>
            </tr>
        </thead>
        <tbody>
            @foreach(App\BerkasVerifikasiData::where('is_active','yes')->get() as $key => $data)
            <tr>
                <td style="padding:7px;border:1px solid black;">{{$key+1}}.</td>
                <td style="padding:7px;border:1px solid black;">{{$data->nama_berkas}}</td>
                <td style="padding:7px;border:1px solid black;"></td>
                <td style="padding:7px;border:1px solid black;"></td>
            </tr>
            @endforeach
        </tbody>
        <font>Selain itu, jangan lupa membawa dokumen aslinya agar petugas sekolah dapat mencocokan keaslian dokumen dengan fotocopy.</font>
    </table> <br>
    <div id="wali">
        <center>
        Menyetujui, Orang tua / <br>
        wali siswa terdaftar.
        <br><br><br><br>
        (................................)</center>
    </div>
    <div id="foto">
        <img src="{{public_path() . '/images/siswa/'.$users->foto}}" style="width:95px; position: relative;"/><br>
        <font style="font-size:12px;">{{$users->name}}</font>
    </div>
    <div id="kepsek">
        <center>
        Kepala Sekolah
        <br><br><br><br><br>
        (................................)</center>
    </div>
    
    
</body>
</html>