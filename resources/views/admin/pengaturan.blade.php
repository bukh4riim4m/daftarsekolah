@extends('layouts.admin.app_baru')
@section('content')
<div class="main-content">
<section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Pengaturan</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
  <section class="container">
      <div style="margin:20px;">
        <div class="row">
          <div class="col-md-12">
          @include('flash::message')
            <div class="row mt-40">
            
              <div class="col-md-10">
              <h4>1. Atur Tanggal Pendaftaran</h4>
                <p>Pilih tanggal berapa dan sampai tanggal berapa pendaftaran onlinennya dibuka.</p>
                <!-- Datepicker Daterange Markup -->
                <form action="{{route('admin-update-pengaturan')}}" method="post">
                <input type="hidden" value="tgl" name="action">
                @csrf
                  <div id="example-daterange">
                  
                    <div class="input-daterange input-group" id="datepicker">
                      
                        <input type="text" class="input-sm form-control" name="mulai" value="{{date('d-m-Y', strtotime($tanggal->mulai))}}"/>
                        <span class="input-group-addon">Sampai</span>
                        <input type="text" class="input-sm form-control" name="selesai" value="{{date('d-m-Y', strtotime($tanggal->selesai))}}" />
                        
                      
                      
                    </div><br>
                    <button class="btn btn-primary">Simpan</button>
                    <!-- Datepicker Daterange Script -->
                    <script type="text/javascript">
                      $('#example-daterange .input-daterange').datepicker({
                      });
                    </script>
                  </div>
                  </form>
              </div>
              
            </div>
            <div class="row mt-40">
              <div class="col-md-10">
              <h4>2. Pengaturan Menu Halaman</h4>
                <p>Aktifkan atau Nonaktifkan Menu.</p>
                <!-- Datepicker Daterange Markup -->
                  <table class="table">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Menu</th>
                        <th>Aktifkan ?</th>
                      </tr>
                    </thead>
                    <tbody>
                      <form action="{{route('admin-update-pengaturan')}}" method="post">
                        <input type="hidden" value="menu" name="action">
                      @csrf
                      @foreach(App\MenuDepan::get() as $keys => $menu)
                      <input type="hidden" name="ids[]" value="{{$menu->id}}">
                      <tr>
                        <td>{{$keys+1}}.</td>
                        <td>{{$menu->menu}}</td>
                        <td>
                          <?php $menus = ['yes','no'];?>
                          <select name="is_active[]">
                            @foreach($menus as $me)
                              @if($menu->is_active == $me)
                                <option value="{{$me}}" selected>{{$me}}</option>
                              @else
                                <option value="{{$me}}">{{$me}}</option>
                              @endif
                            @endforeach
                          </select>
                        </td>
                      </tr>
                      @endforeach
                      <tr>
                        <td></td>
                        <td colspan="2"><button type="submit" class="btn btn-primary">Simpan</button></td>
                      </tr>
                      </form>
                    </tbody>
                  </table>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
</div>
@endsection
