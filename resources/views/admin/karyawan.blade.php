@extends('layouts.admin.app_baru')
@section('content')
<div class="main-content">
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Karyawan - Karyawan</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
    <section id="news">
  <div style="margin:20px;">
    <div class="section-content">
      @include('flash::message')
      <button type="button" class="btn btn-primary btn-lg text-white mt-10" data-toggle="modal" data-target="#myModal2"> Tambah </button> <br><br>
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Tambah Karyawan</h4>
                    </div>
                    <div class="modal-body">
                      <form action="{{route('admin-data-karyawan')}}" method="post" id="proses" enctype="multipart/form-data">
                      <input type="hidden" name="action" value="add">
                      @csrf
                          <div class="form-group">
                            <input type="text" name="nama" value="{{old('nama')}}" placeholder="Nama Karyawan" class="form-control @error('nama') error @enderror">
                            @error('nama')
                            <p class="error">{{ $message }}</p>
                            @enderror
                          </div>
                          <div class="form-group">
                            <input type="text" name="email" value="{{old('email')}}" placeholder="Email Karyawan" class="form-control @error('email') error @enderror">
                            @error('email')
                            <p class="error">{{ $message }}</p>
                            @enderror
                          </div>
                          <div class="form-group">
                            <input type="text" name="hp" value="{{old('hp')}}" placeholder="Nomor HP Karyawan" class="form-control @error('hp') error @enderror">
                            @error('hp')
                            <p class="error">{{ $message }}</p>
                            @enderror
                          </div>
                          <div class="form-group">
                            <input type="file" id="pic" name="foto" style="display:none" onchange="document.getElementById('filename').value=this.value">
                            <input type="text" id="filename" class="@error('foto') error @enderror" disabled>
                            <input type="button" value="Pilih Foto" onclick="document.getElementById('pic').click()">
                            @error('foto')
                            <p class="error">{{ $message }}</p>
                            @enderror
                            <!-- <input type="file" name="foto" class="form-control"> -->
                          </div>
                          <div class="form-group">
                            <input type="text" name="password" value="{{old('password')}}" placeholder="Password Karyawan" class="form-control @error('password') error @enderror">
                            @error('password')
                            <p class="error">{{ $message }}</p>
                            @enderror
                          </div>
                          <hr>
                          <div class="form-group">
                            <input type="text" name="link_fb" value="{{old('link_fb')}}" placeholder="Link Facebook (Tidak wajib)" class="form-control">
                          </div>
                          <div class="form-group">
                            <input type="text" name="instagram" value="{{old('instagram')}}" placeholder="Link Instagram (Tidak wajib)" class="form-control">
                          </div>
                          
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary text-white" onclick="event.preventDefault();
                        document.getElementById('proses').submit();">Save</button>
                    </div>
                  </div>
                </div>
              </div>
        <div class="row mtli-row-clearfix">
        @foreach($karyawan as $key => $kry)
        <div class="col-xs-12 col-sm-6 col-md-3 sm-text-center mb-30 mb-sm-30">
            <div class="team-members maxwidth400">
                <div class="team-thumb">
                    <img class="img-fullwidth" alt="" src="{{asset('images/karyawan/'.$kry->foto)}}">
                </div>
                <div class="team-bottom-part border-bottom-theme-color-2-2px bg-lighter border-1px text-center p-10 pt-20 pb-10">
                    <h4 class="text-uppercase font-raleway font-weight-600 m-0"><a class="text-theme-color-2" href="page-teachers-details.html"> {{$kry->name}}</a></h4>
                    <h5 class="text-theme-color">Karyawan</h5>
                    <ul class="styled-icons icon-sm icon-dark icon-theme-colored">
                        <li><a href=""><i class="fa fa-edit"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
                        <li><a href=""><i class="fa fa-trash"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-skype"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
@endsection
@section('js')
@endsection