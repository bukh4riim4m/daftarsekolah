@extends('layouts.admin.app_baru')
@section('content')
<section class="w3ls-bnrbtm py-5" id="about">
    <div class="container py-xl-5 py-lg-3">
        <h4 class="title-w3 mb-md-5 mb-sm-4 mb-2 text-center font-weight-bold">Data Calon Siswa Yang Daftar</h4>
        <div class="row">
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>No.</th><th>Nama</th><th>No.Hp</th><th>email</th>
                </tr>
              </thead>
              <tbody>
                @foreach($siswas as $key => $siswa)
                <tr>
                  <td>{{$key+1}}.</td><td>{{$siswa->name}}</td><td>{{$siswa->hp}}</td><td>{{$siswa->email}}</td>
                </tr>
                @endforeach
              </tbody>


            </table>
        </div>
    </div>
</section>
@endsection
