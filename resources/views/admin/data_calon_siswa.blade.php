@extends('layouts.admin.app_baru')
@section('content')
<div class="main-content">
<section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Data Calon Siswa</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
  <section id="news">
  <div style="margin:20px;">
    <div class="section-content">
          <div class="col-md-12 col-xs-12">
            <div class="table-responsive">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>No.</th><th>Foto</th><th>Nama</th><th>No.Hp</th><th>Jenis Kelamin</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($siswas as $key => $siswa)
                  <tr>
                    <td>{{$key+1}}.</td><td></td><td>{{$siswa->name}}</td><td>{{$siswa->hp}}</td><td>{{$siswa->jenisKelaminSiswa->jenis_kelamin}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            
          </div>
        </div>
    </div>
  </section>
</div>
@endsection
