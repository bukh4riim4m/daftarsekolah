@extends('layouts.app')
@section('css')

@endsection
@section('content')
<div class="main-content">

<!-- Section: inner-header -->
<section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
  <div class="container pt-70 pb-20">
    <!-- Section Content -->
    <div class="section-content">
      <div class="row">
        <div class="col-md-12">
          <h2 class="title text-white">Berita</h2>
          <ol class="breadcrumb text-left text-black mt-10">
            <li><a href="{{url('/')}}">Beranda</a></li>
            <li class="active text-gray-silver">Berita</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Section: News & Blog -->
<section id="news">
  <div class="container">
    <div class="section-content">
      <div class="row">
      @foreach($datas as $key => $data)
        <div class="col-sm-6 col-md-4">
          <article class="post clearfix mb-30 mb-sm-30">
            <div class="entry-header">
              <div class="post-thumb thumb"> 
                <img src="{{asset('images/blog/'.$data->gambar)}}" alt="" class="img-responsive img-fullwidth"> 
              </div>
            </div>
            <div class="entry-content p-20 pr-10 bg-lighter">
              <div class="entry-meta media mt-0 no-bg no-border">
                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                  <ul>
                    <li class="font-16 text-white font-weight-600 border-bottom">{{date('d',strtotime($data->updated_at))}}</li>
                    <li class="font-12 text-white text-uppercase">{{date('M',strtotime($data->updated_at))}}</li>
                  </ul>
                </div>
                <div class="media-body pl-15">
                  <div class="event-content pull-left flip">
                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">{{substr($data->judul,0,15)}} ...</a></h4>
                    <?php $datas = App\KomentarBerita::where('beritas_id',$data->id)->where('is_active','yes')->get();?>
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> {{count($datas)}} Komentar</span>                       
                    <!-- <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                        -->
                  </div>
                </div>
              </div>
              <p class="mt-10">{!!substr($data->isi,0,30)!!} ...</p>
              <a href="{{url('berita/'.$data->id.'/'.str_replace(' ','-',$data->judul))}}" class="btn-read-more">Read more</a>
              <div class="clearfix"></div>
            </div>
          </article>
        </div>
        @endforeach
        <!-- <div class="col-sm-6 col-md-4">
          <article class="post clearfix mb-30 mb-sm-30">
            <div class="entry-header">
              <div class="post-thumb thumb"> 
                <img src="http://placehold.it/330x225" alt="" class="img-responsive img-fullwidth"> 
              </div>
            </div>
            <div class="entry-content p-20 pr-10 bg-lighter">
              <div class="entry-meta media mt-0 no-bg no-border">
                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                  <ul>
                    <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                    <li class="font-12 text-white text-uppercase">Feb</li>
                  </ul>
                </div>
                <div class="media-body pl-15">
                  <div class="event-content pull-left flip">
                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">Post title here</a></h4>
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                  </div>
                </div>
              </div>
              <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda Pariatur iste.</p>
              <a href="{{url('blog/'.date('d-m-Y').'/judul')}}" class="btn-read-more">Read more</a>
              <div class="clearfix"></div>
            </div>
          </article>
        </div>
        <div class="col-sm-6 col-md-4">
          <article class="post clearfix mb-30 mb-sm-30">
            <div class="entry-header">
              <div class="post-thumb thumb"> 
                <img src="http://placehold.it/330x225" alt="" class="img-responsive img-fullwidth"> 
              </div>
            </div>
            <div class="entry-content p-20 pr-10 bg-lighter">
              <div class="entry-meta media mt-0 no-bg no-border">
                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                  <ul>
                    <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                    <li class="font-12 text-white text-uppercase">Feb</li>
                  </ul>
                </div>
                <div class="media-body pl-15">
                  <div class="event-content pull-left flip">
                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">Post title here</a></h4>
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                  </div>
                </div>
              </div>
              <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda Pariatur iste.</p>
              <a href="{{url('blog/'.date('d-m-Y').'/judul')}}" class="btn-read-more">Read more</a>
              <div class="clearfix"></div>
            </div>
          </article>
        </div>
        <div class="col-sm-6 col-md-4">
          <article class="post clearfix mb-sm-30">
            <div class="entry-header">
              <div class="post-thumb thumb"> 
                <img src="http://placehold.it/330x225" alt="" class="img-responsive img-fullwidth"> 
              </div>
            </div>
            <div class="entry-content p-20 pr-10 bg-lighter">
              <div class="entry-meta media mt-0 no-bg no-border">
                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                  <ul>
                    <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                    <li class="font-12 text-white text-uppercase">Feb</li>
                  </ul>
                </div>
                <div class="media-body pl-15">
                  <div class="event-content pull-left flip">
                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">Post title here</a></h4>
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                  </div>
                </div>
              </div>
              <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda Pariatur iste.</p>
              <a href="{{url('blog/'.date('d-m-Y').'/judul')}}" class="btn-read-more">Read more</a>
              <div class="clearfix"></div>
            </div>
          </article>
        </div>
        <div class="col-sm-6 col-md-4">
          <article class="post clearfix mb-sm-30">
            <div class="entry-header">
              <div class="post-thumb thumb"> 
                <img src="http://placehold.it/330x225" alt="" class="img-responsive img-fullwidth"> 
              </div>
            </div>
            <div class="entry-content p-20 pr-10 bg-lighter">
              <div class="entry-meta media mt-0 no-bg no-border">
                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                  <ul>
                    <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                    <li class="font-12 text-white text-uppercase">Feb</li>
                  </ul>
                </div>
                <div class="media-body pl-15">
                  <div class="event-content pull-left flip">
                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">Post title here</a></h4>
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                  </div>
                </div>
              </div>
              <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda Pariatur iste.</p>
              <a href="{{url('blog/'.date('d-m-Y').'/judul')}}" class="btn-read-more">Read more</a>
              <div class="clearfix"></div>
            </div>
          </article>
        </div>
        <div class="col-sm-6 col-md-4">
          <article class="post clearfix mb-sm-30">
            <div class="entry-header">
              <div class="post-thumb thumb"> 
                <img src="http://placehold.it/330x225" alt="" class="img-responsive img-fullwidth"> 
              </div>
            </div>
            <div class="entry-content p-20 pr-10 bg-lighter">
              <div class="entry-meta media mt-0 no-bg no-border">
                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                  <ul>
                    <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                    <li class="font-12 text-white text-uppercase">Feb</li>
                  </ul>
                </div>
                <div class="media-body pl-15">
                  <div class="event-content pull-left flip">
                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">Post title here</a></h4>
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                  </div>
                </div>
              </div>
              <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda Pariatur iste.</p>
              <a href="{{url('blog/'.date('d-m-Y').'/judul')}}" class="btn-read-more">Read more</a>
              <div class="clearfix"></div>
            </div>
          </article>
        </div> -->
      </div>
    </div>
  </div>
</section>
</div>
@endsection
@section('js')

@endsection