@extends('layouts.app')
@section('css')

@endsection
@section('content')
<div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-70 pb-20">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <h2 class="title text-white">Detail Berita</h2>
              <ol class="breadcrumb text-left text-black mt-10">
                <li><a href="{{url('/')}}">Beranda</a></li>
                <li class="active text-gray-silver">Detail Berita</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Blog -->
    <section>
    <?php $datas = App\KomentarBerita::where('beritas_id',$data->id)->where('is_active','yes')->get();?>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
          <div class="col-md-9">
            <div class="blog-posts single-post">
              <article class="post clearfix mb-0">
                <div class="entry-header">
                  <div class="post-thumb thumb"> <img src="{{asset('images/blog/'.$data->gambar)}}" alt="" class="img-responsive img-fullwidth"> </div>
                </div>
                <div class="entry-content">
                  <div class="entry-meta media no-bg no-border mt-15 pb-20">
                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                      <ul>
                        <li class="font-16 text-white font-weight-600">{{date('d',strtotime($data->updated_at))}}</li>
                        <li class="font-12 text-white text-uppercase">{{date('M',strtotime($data->updated_at))}}</li>
                      </ul>
                    </div>
                    <div class="media-body pl-15">
                      <div class="event-content pull-left flip">
                        <h3 class="entry-title text-white text-uppercase pt-0 mt-0"><a href="blog-single-right-sidebar.html">{{$data->judul}}</a></h3>
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> {{count($datas)}} Komentar</span>                       
                        <!-- <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span> -->
                      </div>
                    </div>
                  </div>
                  <p class="mb-15">{!!$data->isi!!}</p>
                  <div class="mt-30 mb-0">
                    <h5 class="pull-left flip mt-10 mr-20 text-theme-colored">Share:</h5>
                    <ul class="styled-icons icon-circled m-0">
                      <li><a href="#" data-bg-color="#3A5795"><i class="fa fa-facebook text-white"></i></a></li>
                      <!-- <li><a href="#" data-bg-color="#55ACEE"><i class="fa fa-twitter text-white"></i></a></li>
                      <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus text-white"></i></a></li> -->
                    </ul>
                  </div>
                </div>
              </article>
              <div class="tagline p-0 pt-20 mt-5">
                <div class="row">
                  <div class="col-md-8">
                    <div class="tags">
                      <p class="mb-0"><i class="fa fa-tags text-theme-colored"></i> <span>Komentar:</span> </p>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="share text-right flip">
                      <p><i class="fa fa-share-alt text-theme-colored"></i> Share</p>
                    </div>
                  </div>
                </div>
              </div>
              @include('flash::message')
              @foreach($datas as $key => $umum) 
              <div class="author-details media-post">
                <a href="#" class="post-thumb mb-0 pull-left flip pr-20"><img class="img-thumbnail" width="50px" alt="" src="https://graph.facebook.com/v2.6/{{$umum->userBeritaId->provider_id}}/picture?type=normal" /></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-0"><a href="#" class="font-18">{{$umum->userBeritaId->name}}</a></h5>
                  <p>{{$umum->komentar}}</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <hr>
              @endforeach

              <div id="comments" class="comments-area pt-50">
                
                <!-- Facebook Comments plugin -->
                <!-- <div id="fb-root"></div> -->
                <!-- <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=539719300102547";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script> -->

                <h3 id="comments-title"><span><fb:comments-count href="http://pc1/themeforest/thememascot/godown-latest/demo/blog-single-facebook-comments.html"></fb:comments-count></span> Komentar</h3>
                
                @if(Auth::guard('komen')->check())
                <form action="{{url('berita/'.$data->id.'/'.str_replace(' ','-',$data->judul))}}" method="post">
                @csrf
                <textarea name="komentar" id="" cols="30" rows="6" class="form-control @error('komentar') error @enderror"></textarea>
                @error('komentar')
                <p class="error">{{ $message }}</p>
                @enderror
                     <br>
                <button class="btn btn-success">Kirim sebagai {{Auth::guard('komen')->user()->name}}</button> 
                <a href="{{url('logout/komen')}}"class="btn btn-danger">Keluar</a> 
                </form>
                @else
                <a href="{{ url('/auth/facebook') }}" class="btn btn-primary">Klik Login dengan Facebook</a><br><br>
                @endif
                <!-- <div class="fb-comments" data-href="http://pc1/themeforest/thememascot/godown-latest/demo/blog-single-facebook-comments.html" data-numposts="5"></div> -->
                <!-- Facebook Comments end -->

              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="sidebar sidebar-right mt-sm-30">
              <div class="widget">
                <h5 class="widget-title line-bottom">Search box</h5>
                <div class="search-form">
                  <form>
                    <div class="input-group">
                      <input type="text" placeholder="Click to Search" class="form-control search-input">
                      <span class="input-group-btn">
                      <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
              {{--<div class="widget">
                <h5 class="widget-title line-bottom">Categories</h5>
                <div class="categories">
                  <ul class="list list-border angle-double-right">
                    <li><a href="#">Creative<span>(19)</span></a></li>
                    <li><a href="#">Portfolio<span>(21)</span></a></li>
                    <li><a href="#">Fitness<span>(15)</span></a></li>
                    <li><a href="#">Gym<span>(35)</span></a></li>
                    <li><a href="#">Personal<span>(16)</span></a></li>
                  </ul>
                </div>
              </div>--}}
              <div class="widget">
                <h5 class="widget-title line-bottom">Berita Terbaru</h5>
                <div class="latest-posts">
                <?php $terbarus = App\Berita::where('is_active','yes')->orderBy('id','DESC')->paginate(5);?>
                @foreach($terbarus as $keys =>$terbaru)
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="{{url('berita/'.$terbaru->id.'/'.str_replace(' ','-',$terbaru->judul))}}"><img width="100px" src="{{asset('images/blog/'.$terbaru->gambar)}}" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="{{url('berita/'.$terbaru->id.'/'.str_replace(' ','-',$terbaru->judul))}}">{{substr($terbaru->judul,0,15)}}...</a></h5>
                      <p>{!!substr($terbaru->isi,0,30)!!}...</p>
                    </div>
                  </article>
                  @endforeach
                  <!-- <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">Industrial Coatings</a></h5>
                      <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                    </div>
                  </article>
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="#"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="#">Storefront Installations</a></h5>
                      <p>Lorem ipsum dolor sit amet adipisicing elit...</p>
                    </div>
                  </article> -->
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Photos from Flickr</h5>
                <div id="flickr-feed" class="clearfix">
                  <!-- Flickr Link -->
                  <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=52617155@N08">
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> 
  </div>  
@endsection
@section('js')
<!-- <script src="//code.jquery.com/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
  $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script> -->
@endsection