@extends('layouts.petugas.app_baru')
@section('content')
<div class="main-content">
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Gallery</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
	
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
          <button class="pull-right btn btn-primary text-white" data-toggle="modal" data-target=".bs-example-modal-sm">Tambah</button>
          <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel3">Tambah Gambar</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('petugas-gallery')}}" method="post" enctype="multipart/form-data" id="proses-gallery">
                        @csrf

                        Gambar  <br>
                        <input  class="form-control" type="file" name="gambar">
                        Kategori <br>
                        <select name="kategori" id="" class="form-control">
                            <option value="">Pilih</option>
                            <option value="select1">Kegiatan</option>
                            <option value="select2">Sekolah</option>
                            <option value="select3">Pelajar</option>
                        </select>
                        </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary text-white"  onclick="event.preventDefault();
                        document.getElementById('proses-gallery').submit();">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            <!-- Works Filter -->
            <div class="portfolio-filter font-alt align-center">
              <a href="#" class="active" data-filter="*">Semua</a>
              <a href="#select1" class="" data-filter=".select1">Kegiatan</a>
              <a href="#select2" class="" data-filter=".select2">Sekolah</a>
              <a href="#select3" class="" data-filter=".select3">Pelajar</a>
            </div>
            <!-- End Works Filter -->
            
            <!-- Portfolio Gallery Grid -->
            <div id="grid" class="gallery-isotope grid-4 gutter clearfix">

              <!-- Portfolio Item Start -->
              @foreach($galleries as $key => $galleri)
              <div class="gallery-item {{$galleri->kategori}}">
                <div class="thumb">
                    <img class="img-fullwidth" src="{{asset('images/galleries/'.$galleri->gambar)}}" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a data-lightbox="image" href="{{asset('images/galleries/'.$galleri->gambar)}}"><i class="fa fa-plus"></i></a>
                        <a href="#"><i class="fa fa-edit"></i></a>
                        <a href="#"><i class="fa fa-trash"></i></a>
                      </div>
                    </div>
                  </div>
                  <a class="hover-link" data-lightbox="image" href="{{asset('images/galleries/'.$galleri->gambar)}}">View more</a>
                </div>
              </div>
              @endforeach

            </div>
            <!-- End Portfolio Gallery Grid -->
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
@endsection
@section('js')
@endsection