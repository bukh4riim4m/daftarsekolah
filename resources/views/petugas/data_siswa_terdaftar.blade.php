@extends('layouts.petugas.app_baru')
@section('content')
<div class="main-content">
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Data Calon Siswa Terdaftar</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
	<section id="news">
  <div style="margin:20px;">
    <div class="section-content">
        <button class="pull-right btn btn-primary text-white" data-toggle="modal" data-target=".bs-example-modal-lg">Download <i class="fa fa-download"></i></button><br><br>

        <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Foto</th>
                    <th>Nomor Pendaftaran</th>
                    <th>Tgl Daftar</th>
                    <th>Nama Lengkap</th>
                    <th>Tempat, Tgl Lahir</th>
                    <th>Nomor Telpon</th>
                    <th>Jenis Kelamin</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($datas as $key => $data)
                <tr>
                    <td>{{$key+1}}.</td>
                    <td><img src="{{asset('images/siswa/'.$data->foto)}}" width="40px" alt=""></td>
                    <td>{{$data->nomor_pendaftaran}}</td>
                    <td>{{date('d-m-Y', strtotime($data->created_at))}}</td>
                    <td>{{$data->name}}</td>
                    <td>{{$data->tempat_lahir}}, {{date('d-m-Y', strtotime($data->tgl_lahir))}}</td>
                    <td>{{$data->hp}}</td>
                    <td>{{$data->jenisKelaminSiswa->jenis_kelamin}}</td>
                    <td><i class="fa fa-search"></i> | <i class="fa fa-edit"></i> | <i class="fa fa-trash"></i> </td>
                </tr>
                @endforeach
            </tbody>
            
        </table>
        </div>
          </div>
        </div>
    
    </section>

  </div>
  <!-- end main-content -->
@endsection
@section('js')
@endsection