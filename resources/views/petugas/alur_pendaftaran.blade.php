@extends('layouts.petugas.app_baru')
@section('content')
<div class="main-content">
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Alur Pendaftaran</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
	<section class="container">
        <div class="container  pt-25 pb-5">
        @include('flash::message')
      <button class="pull-right btn btn-primary text-white" data-toggle="modal" data-target=".bs-example-modal-lg">Tambah</button><br><br>
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel2">Tambah</h4>
              </div>
              <div class="modal-body">
                <form action="{{route('petugas-alur-pendaftaran')}}" method="post" id="proses">
                @csrf
                  <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label class="col-form-label">Judul</label>
                        <input type="text" name="judul" placeholder="Judul" class="form-control @error('judul') error @enderror" value="{{old('judul')}}">
                        @error('judul')
                        <p class="error">{{ $message }}</p>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Tgl Mulai & Selesai</label>
                            <div id="example-daterange">
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="tgl_mulai" />
                                    <span class="input-group-addon">S/d</span>
                                    <input type="text" class="input-sm form-control" name="tgl_selesai" />
                                </div>
                                <!-- Datepicker Daterange Script -->
                                <script type="text/javascript">
                                    $('#example-daterange .input-daterange').datepicker({
                                    });
                                </script>
                            </div>
                            @error('judul')
                            <p class="error">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-form-label">Isi Penjelasan</label>
                        <textarea name="penjelasan" id="" cols="30" rows="10" placeholder="Isi penjelasan" class="form-control @error('penjelasan') error @enderror">{{old('penjelasan')}}</textarea>
                        @error('penjelasan')
                        <p class="error">{{ $message }}</p>
                        @enderror
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary text-white"  onclick="event.preventDefault();
                        document.getElementById('proses').submit();">Simpan</button>
              </div>
            </div>
          </div>
        </div>
        <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Judul</th>
                    <th>Penjelasan</th>
                    <th>Tgl Mulai</th>
                    <th>Tgl Selesai</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($datas as $key => $data)
                <tr>
                    <td>{{$key+1}}.</td>
                    <td>{{$data->judul}}</td>
                    <td>{{$data->keterangan}}</td>
                    <td>{{date('d-m-Y', strtotime($data->tgl_mulai))}}</td>
                    <td>{{date('d-m-Y', strtotime($data->tgl_selesai))}}</td>
                    <td><i class="fa fa-edit"></i> | <i class="fa fa-trash"></i></td>
                </tr>
                @endforeach
            </tbody>
            
        </table>
        </div>
        
        </div>
    
    </section>

  </div>
  <!-- end main-content -->
@endsection
@section('js')
@endsection