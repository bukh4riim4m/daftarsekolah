@extends('layouts.petugas.app_baru')
@section('content')
<div class="main-content">
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Guru - Guru</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
  <section id="news">
  <div style="margin:20px;">
    <div class="section-content">
      @include('flash::message')
      <button type="button" class="btn btn-primary btn-lg text-white mt-10" data-toggle="modal" data-target="#myModal2"> Tambah </button> <br><br>
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Tambah Guru</h4>
                    </div>
                    <div class="modal-body">
                      <form action="{{route('petugas-guru')}}" method="post" id="proses-guru" enctype="multipart/form-data">
                      <input type="hidden" name="action" value="add">
                      @csrf
                          <div class="form-group">
                            <input type="text" name="nama" placeholder="Nama Guru" class="form-control">
                          </div>
                          <div class="form-group">
                            <input type="text" name="hp" placeholder="Nomor HP" class="form-control">
                          </div>
                          <div class="form-group">
                            <input type="email" name="email" placeholder="Email" class="form-control">
                          </div>
                          <div class="form-group">
                            <input type="text" name="link_fb" placeholder="Link Facebook" class="form-control">
                          </div>
                          <div class="form-group">
                            <input type="file" id="pic" name="foto" style="display:none" onchange="document.getElementById('filename').value=this.value">
                            <input type="text" id="filename" disabled>
                            <input type="button" value="Pilih Foto" onclick="document.getElementById('pic').click()">
                            <!-- <input type="file" name="foto" class="form-control"> -->
                          </div>
                          <div class="form-group">
                            <select name="kategori" id="" class="form-control">
                                <option value="">Mengajar</option>
                                @foreach(App\KategoriGuru::where('is_active','yes')->get() as $key => $kategories)
                                <option value="{{$kategories->id}}">{{$kategories->kategori}}</option>
                                @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                              <textarea name="kata_mutiara" id="" cols="30" rows="6" class="form-control" placeholder="Kata Mutiara"></textarea>
                          </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary text-white" onclick="event.preventDefault();
                        document.getElementById('proses-guru').submit();">Save</button>
                    </div>
                  </div>
                </div>
              </div>
        <div class="row mtli-row-clearfix">
        @foreach($gurus as $key => $guru)
          <div class="col-xs-12 col-sm-6 col-md-3 sm-text-center mb-30 mb-sm-30">
            <div class="team maxwidth400">
              <div class="thumb"><img class="img-fullwidth" src="{{asset('images/guru/'.$guru->foto)}}" alt=""></div>
              <div class="content border-1px border-bottom-theme-color-2-2px p-15 bg-light clearfix">
                <h4 class="name text-theme-color-2 mt-0 text-center">{{$guru->name}} <br><small>({{$guru->KategoriGuru->kategori}})</small></h4>
                <p class="mb-20" style="text-align:justify;"><br>{{substr($guru->kata_mutiara,0,25)}} .....</p>
                <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-left flip">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a data-toggle="modal" data-target="#edit{{$guru->id}}"><i class="fa fa-edit"></i></a></li>
                  <li><a href="#"><i class="fa fa-trash"></i></a></li>
                </ul>
                <a class="btn btn-theme-colored btn-sm pull-right flip" href="{{route('petugas-detail-guru',$guru->id)}}">view details</a>
              </div>
            </div>
          </div>
          <div class="modal fade" id="edit{{$guru->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">Edit Data Guru</h4>
                    </div>
                    <div class="modal-body">
                      <form action="{{route('petugas-guru')}}" method="post" id="proses-guru" enctype="multipart/form-data">
                      <input type="hidden" name="action" value="edit">
                      @csrf
                          <div class="form-group row">
                              <div class="col-md-6">
                                <input type="text" name="nama" placeholder="Nama Guru" value="{{$guru->name}}" class="form-control">
                              </div>
                              <div class="col-md-6">
                                <input type="text" name="hp" placeholder="Nomor HP" value="{{$guru->hp}}" class="form-control">
                              </div>
                            
                          </div>
                          <div class="form-group row">
                            <div class="col-md-6">
                            <input type="email" name="email" placeholder="Email" value="{{$guru->email}}" class="form-control">
                              </div>
                              <div class="col-md-6">
                              <input type="text" name="link_fb" placeholder="Link Facebook" value="{{$guru->link_fb}}" class="form-control">
                              </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-6">
                            <select name="kategori" id="" class="form-control">
                                <option value="">Mengajar</option>
                                @foreach(App\KategoriGuru::where('is_active','yes')->get() as $key => $kategories)
                                    @if($kategories->id == $guru->kategori_id)
                                    <option value="{{$kategories->id}}" selected>{{$kategories->kategori}}</option>
                                    @else
                                    <option value="{{$kategories->id}}">{{$kategories->kategori}}</option>
                                    @endif
                                @endforeach
                            </select>
                            
                            </div>
                            <div class="col-md-6">
                            <input type="file" id="pic{{$guru->id}}" name="foto" style="display:none" onchange="document.getElementById('filename{{$guru->id}}').value=this.value">
                            <input type="text" id="filename{{$guru->id}}" disabled>
                            <input type="button" class="btn btn-success" value="Pilih Foto" onclick="document.getElementById('pic{{$guru->id}}').click()">
                            

                            </div>
                          </div>
                          <div class="form-group">
                          <input type="text" name="pengalaman" placeholder="Pengalaman" value="{{$guru->pengalaman}}" class="form-control">
                          </div>
                          <div class="form-group">
                          <input type="text" name="alamat" placeholder="Alamat Guru" value="{{$guru->alamat}}" class="form-control">
                          </div>
                          <div class="form-group">
                              <textarea name="kata_mutiara" id="" cols="30" rows="6" class="form-control" placeholder="Kata Mutiara">{{$guru->kata_mutiara}}</textarea>
                          </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary text-white" onclick="event.preventDefault();
                        document.getElementById('proses-guru').submit();">Save</button>
                    </div>
                  </div>
                </div>
              </div>
          @endforeach
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
@endsection
@section('js')
@endsection