@extends('layouts.petugas.app_baru')
@section('content')
<div class="main-content">
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Berita</h6>
			 
            </div>
          </div>
        </div>
	  </div>
  </section>
  <section id="news">
  <div style="margin:20px;">
    <div class="section-content">
      <div class="row">
      @include('flash::message')
      <div class="col-sm-12 col-md-12">
        @include('flash::message')
        <button class="pull-right btn btn-primary text-white" data-toggle="modal" data-target=".bs-example-modal-lg">Tambah</button>
          <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel2">Tambah Berita</h4>
                </div>
                <div class="modal-body">
                  <form action="{{route('petugas-create-berita')}}" method="post" enctype="multipart/form-data" id="proses">
                  @csrf
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-form-label">Judul Berita</label>
                          <input type="text" name="judul" placeholder="Judul Berita" class="form-control @error('judul') error @enderror" value="{{old('judul')}}">
                          @error('judul')
                          <p class="error">{{ $message }}</p>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-form-label">Gambar</label>
                          <input type="file" name="gambar" class="form-control  @error('gambar') error @enderror">
                          @error('gambar')
                          <p class="error">{{ $message }}</p>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="col-form-label">Isi Berita</label>
                          <textarea name="isi" id="" cols="30" rows="10" placeholder="Isi berita" class="form-control @error('isi') error @enderror">{{old('isi')}}</textarea>
                          @error('isi')
                          <p class="error">{{ $message }}</p>
                          @enderror
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary text-white"  onclick="event.preventDefault();
                          document.getElementById('proses').submit();">Save changes</button>
                </div>
              </div>
            </div>
          </div>
      </div>
      <hr>
      @foreach($datas as $key => $data)
        <div class="col-sm-6 col-md-4">
          <article class="post clearfix mb-30 mb-sm-30">
            <div class="entry-header">
              <div class="post-thumb thumb"> 
                <img src="{{asset('images/blog/'.$data->gambar)}}" alt="" class="img-responsive img-fullwidth"> 
              </div>
            </div>
            <div class="entry-content p-20 pr-10 bg-lighter">
              <div class="entry-meta media mt-0 no-bg no-border">
                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                  <ul>
                    <li class="font-16 text-white font-weight-600 border-bottom">{{date('d',strtotime($data->updated_at))}}</li>
                    <li class="font-12 text-white text-uppercase">{{date('M',strtotime($data->updated_at))}}</li>
                  </ul>
                </div>
                <div class="media-body pl-15">
                  <div class="event-content pull-left flip">
                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="#">{{substr($data->judul,0,15)}} ...</a></h4>
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                  </div>
                </div>
              </div>
              <p class="mt-10">{!!substr($data->isi,0,30)!!} ...</p>
              <a href="{{url('blog/'.date('d-m-Y').'/judul')}}" class="btn-read-more">Read more</a>
              <div class="clearfix"></div>
            </div>
          </article>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
  </div>
  <!-- end main-content -->
@endsection
@section('js')
@endsection