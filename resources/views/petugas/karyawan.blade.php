@extends('layouts.petugas.app_baru')
@section('content')
<div class="main-content">
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
      <div class="container pt-25 pb-5">
        <!-- Section Content -->
        <div class="content">
          <div class="row">
            <div class="col-md-12">
			  <h6 class="title text-white" style="margin-top:60px;font-size:20px;">Karyawan - Karyawan</h6>
			 
            </div>
          </div>
        </div>
	  </div>
	</section>
  <section id="news">
  <div style="margin:20px;">
    <div class="section-content">
        <div class="row mtli-row-clearfix">
        @foreach($karyawan as $key => $guru)
        <div class="col-xs-12 col-sm-6 col-md-3 sm-text-center mb-30 mb-sm-30">
            <div class="team-members maxwidth400">
                <div class="team-thumb">
                    <img class="img-fullwidth" alt="" src="{{asset('images/karyawan/'.$guru->foto)}}">
                </div>
                <div class="team-bottom-part border-bottom-theme-color-2-2px bg-lighter border-1px text-center p-10 pt-20 pb-10">
                    <h4 class="text-uppercase font-raleway font-weight-600 m-0"><a class="text-theme-color-2" href="page-teachers-details.html"> {{$guru->name}}</a></h4>
                    <h5 class="text-theme-color">Karyawan</h5>
                    <ul class="styled-icons icon-sm icon-dark icon-theme-colored">
                        <li><a href="{{$guru->link_fb}}"><i class="fa fa-facebook"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
                        <li><a href="{{$guru->instagram}}"><i class="fa fa-instagram"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-skype"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
@endsection
@section('js')
@endsection