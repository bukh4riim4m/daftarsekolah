<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>TT</title>
        <link rel="shortcut icon" type="image/x-icon" href="">
		     <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">
    		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/select2.min.css')}}">
    		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}">
    		<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/summernote/dist/summernote.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/dataTables.bootstrap.min.css')}}">
		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/fullcalendar.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/dataTables.bootstrap.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/morris/morris.css')}}">
    <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- <script src="path/to/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script> -->
    <style>
    .bd-example-modal-lg .modal-dialog{
      display: table;
      position: relative;
      margin: 0 auto;
      text-align:center;
      top: calc(50% - 24px);
    }
    .bd-example-modal-lg .modal-dialog .modal-content{
      background-color: #ffffff;
      border: none;
    }
    </style>
    </head>
    <body>
        <div class="main-wrapper">
          <!-- HEADER -->
      @include('includes.petugas.header')
      <!-- ENDHEADER -->
      <!-- SIDEBAR -->
      @include('includes.petugas.sidebar')
      <!-- ENDSIDEBAR -->
      @yield('content')
      @include('includes.admin.footer')