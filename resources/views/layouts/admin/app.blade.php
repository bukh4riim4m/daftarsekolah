<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        
        <?php $web = App\Banner::find(1); ?>
        <title>{{ $web->title }}</title>
        <link rel="shortcut icon" type="image/x-icon" href="">
		    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
        @yield('css')
        <link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/morris/morris.css')}}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <style>
        .bd-example-modal-lg .modal-dialog{
          display: table;
          position: relative;
          margin: 0 auto;
          text-align:center;
          top: calc(50% - 24px);
        }
        .bd-example-modal-lg .modal-dialog .modal-content{
          background-color: #ffffff;
          border: none;
        }
        </style>
        <!-- <script src="path/to/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script> -->
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets/css/select2.min.css')}}"> -->
    		<!-- <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}"> -->
        <!-- <link rel="stylesheet" type="text/css" href="{{asset('assets/css/dataTables.bootstrap.min.css')}}"> -->
        <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
    </head>
    <body>
        <div class="main-wrapper">
          <!-- HEADER -->
      @include('includes.admin.header')
      <!-- ENDHEADER -->
      <!-- SIDEBAR -->
      @include('includes.admin.sidebar')
      <!-- ENDSIDEBAR -->
      @yield('content')
      

<div class="sidebar-overlay" data-reff="#sidebar"></div>

<script type='text/javascript'>
//<![CDATA[
//Script Redirect CTRL + U
//https://mastamvan.blogspot.com/ ganti dengan url blog kalian
function redirectCU(e) {
  if (e.ctrlKey && e.which == 85) {
    alert('Tidak diperbolehkan CTRL+U');
    return false;
  }
}
document.onkeydown = redirectCU;

// //Script Redirect Klik Kanan
// function redirectKK(e) {
//   if (e.which == 3) {
//     alert('Tidak diperbolehkan Klik Kanan');
//     return false;
//   }
// }
// document.oncontextmenu = redirectKK;
//]]>
</script>
@yield('javascript')
<!-- <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('assets/js/dataTables.bootstrap.min.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script> -->
<!-- <script src="//code.jquery.com/jquery.js"></script> -->
<!-- ==== -->
<!-- <script type="text/javascript" src="{{asset('plugins/morris/morris.min.js')}}"></script> -->
<!-- <script type="text/javascript" src="{{asset('assets/js/select2.min.js')}}"></script> -->

</body>
</html>
