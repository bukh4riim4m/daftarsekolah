@extends('layouts.app')
@section('css')

@endsection
@section('content')
<div class="main-content">

<!-- Section: inner-header -->
<section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{asset('images/sliders/slider_1.jpg')}}">
  <div class="container pt-70 pb-20">
    <!-- Section Content -->
    <div class="section-content">
      <div class="row">
        <div class="col-md-12">
          <h2 class="title text-white">Galleri</h2>
          <ol class="breadcrumb text-left text-black mt-10">
            <li><a href="{{url('/')}}">Beranda</a></li>
            <li class="active text-gray-silver">Galleri</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- Works Filter -->
        <div class="portfolio-filter font-alt align-center">
          <a href="#" class="active" data-filter="*">Semua</a>
          <a href="#select1" class="" data-filter=".select1">Kegiatan</a>
          <a href="#select2" class="" data-filter=".select2">Sekolah</a>
          <a href="#select3" class="" data-filter=".select3">Pelajar</a>
        </div>
        <!-- End Works Filter -->
        
        <!-- Portfolio Gallery Grid -->
        <div id="grid" class="gallery-isotope grid-3 gutter clearfix">

          <!-- Portfolio Item Start -->
          @foreach($galleries as $key => $galleri)
          <div class="gallery-item {{$galleri->kategori}}">
            <div class="thumb">
                <img class="img-fullwidth" src="{{asset('images/galleries/'.$galleri->gambar)}}" alt="project">
              <div class="overlay-shade"></div>
              <div class="icons-holder">
                <div class="icons-holder-inner">
                  <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                    <a data-lightbox="image" href="{{asset('images/galleries/'.$galleri->gambar)}}"><i class="fa fa-plus"></i></a>
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div>
                </div>
              </div>
              <a class="hover-link" data-lightbox="image" href="{{asset('images/galleries/'.$galleri->gambar)}}">View more</a>
            </div>
          </div>
          @endforeach
          <!-- Portfolio Item End -->
          
        </div>
        <!-- End Portfolio Gallery Grid -->
      </div>
    </div>
  </div>
</section>

</div>
@endsection
@section('js')
@endsection