<?php
Route::get('/print', ['as'=>'index','uses'=>'LoginController@print']);


Route::get('/', ['as'=>'index','uses'=>'LoginController@index']);

Auth::routes();
Route::get('/masuk', ['as'=>'masuk','uses'=>'LoginController@getLogin'])->middleware('guest');
Route::post('/masuk', ['as'=>'masuk','uses'=>'LoginController@postLogin'])->middleware('guest');
Route::get('/daftar', ['as'=>'daftar','uses'=>'LoginController@daftar'])->middleware('guest');
Route::post('/daftar', ['as'=>'daftar','uses'=>'LoginController@postdaftar'])->middleware('guest');

Route::get('/kursus', ['as'=>'kursus','uses'=>'LoginController@kursus']);
Route::get('/kursus/{id}', ['as'=>'detail_kursus','uses'=>'LoginController@detailkursus']);

Route::get('/guru-guru', ['as'=>'guru-guru','uses'=>'LoginController@guru']);
Route::get('/guru/{id}', ['as'=>'detail-guru','uses'=>'LoginController@detailguru']);
Route::get('/karyawan', ['as'=>'karyawan','uses'=>'LoginController@karyawan']);
Route::get('/galleri', ['as'=>'gallery','uses'=>'LoginController@gallery']);
Route::get('/berita', ['as'=>'blog','uses'=>'LoginController@blog']);
Route::get('/berita/{tgl}/{judul}', ['as'=>'detail_blog','uses'=>'LoginController@detailblog']);
Route::get('/sejarah_sekolah', ['as'=>'sejarah_sekolah','uses'=>'LoginController@sejarahsekolah']);
Route::get('/visi_misi', ['as'=>'visi_misi','uses'=>'LoginController@visimisi']);
Route::get('/struktur_organisasi', ['as'=>'struktur_organisasi','uses'=>'LoginController@strukturorganisasi']);
Route::get('/kultur_sekolah', ['as'=>'kultur_sekolah','uses'=>'LoginController@kultursekolah']);
Route::get('/layanan_pendidikan', ['as'=>'layanan_pendidikan','uses'=>'LoginController@layananpendidikan']);
Route::get('/ruang_kelas', ['as'=>'ruang_kelas','uses'=>'LoginController@ruangkelas']);
Route::get('/ruang_perpustakaan', ['as'=>'ruang_perpustakaan','uses'=>'LoginController@ruangperpustakaan']);
Route::get('/ruang_laboraturiumn', ['as'=>'ruang_laboraturiumn','uses'=>'LoginController@laboraturiumn']);
Route::get('/aula_sekolah', ['as'=>'aula_sekolah','uses'=>'LoginController@aulasekolah']);
Route::get('/mesjid', ['as'=>'mesjid','uses'=>'LoginController@mesjid']);
Route::get('/front_office', ['as'=>'front_office','uses'=>'LoginController@frontoffice']);
Route::get('/kantin_sekolah', ['as'=>'kantin_sekolah','uses'=>'LoginController@kantinsekolah']);
Route::get('/taman_sekolah', ['as'=>'taman_sekolah','uses'=>'LoginController@tamansekolah']);
Route::get('/toilet', ['as'=>'toilet','uses'=>'LoginController@toilet']);
Route::get('/bus_sekolah', ['as'=>'bus_sekolah','uses'=>'LoginController@bussekolah']);

Route::get('/e-kurikulum', ['as'=>'e_kurikulum','uses'=>'LoginController@kurikulum']);
Route::get('/e-penilaian', ['as'=>'e_penilaian','uses'=>'LoginController@penilaian']);
Route::get('/e-rapor', ['as'=>'e_rapor','uses'=>'LoginController@rapor']);
Route::get('/dapodik', ['as'=>'dapodik','uses'=>'LoginController@dapodik']);
Route::get('/si-lulus', ['as'=>'si_lulus','uses'=>'LoginController@lulus']);
Route::get('/e-learning', ['as'=>'e_learning','uses'=>'LoginController@learning']);

Route::get('/kontak', ['as'=>'kontak','uses'=>'LoginController@kontak']);



// ROUTE SISWA
Route::get('siswa', ['as'=>'siswa','uses'=>'SiswaController@index'])->middleware('auth:user');
Route::post('siswa', ['as'=>'siswa_download_berkas','uses'=>'SiswaController@download'])->middleware('auth:user');
Route::get('siswa/data_saya', ['as'=>'data_saya','uses'=>'SiswaController@datasaya'])->middleware('auth:user');
Route::get('siswa/edit_data', ['as'=>'siswa_edit_data','uses'=>'SiswaController@edit'])->middleware('auth:user');
Route::post('siswa/edit_data', ['as'=>'siswa_edit_data','uses'=>'SiswaController@update'])->middleware('auth:user');
Route::get('siswa/alur_pendaftaran', ['as'=>'siswa-alur-pendaftaran','uses'=>'SiswaController@alur'])->middleware('auth:user');


//ROUTE PETUGAS
Route::get('petugas', ['as'=>'petugas','uses'=>'PetugasController@index'])->middleware('auth:petugas');
Route::get('petugas/gallery', ['as'=>'petugas-gallery','uses'=>'PetugasController@gallery'])->middleware('auth:petugas');
Route::post('petugas/gallery', ['as'=>'petugas-gallery','uses'=>'PetugasController@gallery'])->middleware('auth:petugas');
Route::get('petugas/guru-guru', ['as'=>'petugas-guru','uses'=>'PetugasController@guru'])->middleware('auth:petugas');
Route::post('petugas/guru-guru', ['as'=>'petugas-guru','uses'=>'PetugasController@guru'])->middleware('auth:petugas');
Route::get('petugas/guru/{id}', ['as'=>'petugas-detail-guru','uses'=>'PetugasController@detailguru'])->middleware('auth:petugas');
Route::get('petugas/karyawan', ['as'=>'petugas-karyawan','uses'=>'PetugasController@karyawan'])->middleware('auth:petugas');
Route::post('petugas/karyawan', ['as'=>'petugas-karyawan','uses'=>'PetugasController@karyawan'])->middleware('auth:petugas');
Route::get('petugas/pengaturan', ['as'=>'petugas-pengaturan','uses'=>'PetugasController@pengaturan'])->middleware('auth:petugas');
Route::get('petugas/berita', ['as'=>'petugas-berita','uses'=>'PetugasController@berita'])->middleware('auth:petugas');
Route::post('petugas/berita', ['as'=>'petugas-berita','uses'=>'PetugasController@berita'])->middleware('auth:petugas');
Route::get('petugas/form-berita', ['as'=>'petugas-form-berita','uses'=>'PetugasController@formberita'])->middleware('auth:petugas');
Route::post('petugas/create-berita', ['as'=>'petugas-create-berita','uses'=>'PetugasController@createberita'])->middleware('auth:petugas');
Route::get('petugas/alur-pendaftaran', ['as'=>'petugas-alur-pendaftaran','uses'=>'PetugasController@alurpendaftaran'])->middleware('auth:petugas');
Route::post('petugas/alur-pendaftaran', ['as'=>'petugas-alur-pendaftaran','uses'=>'PetugasController@alurpendaftaran'])->middleware('auth:petugas');
Route::get('petugas/calon-siswa-terdaftar', ['as'=>'petugas-calon-siswa-terdaftar','uses'=>'PetugasController@calonsiswa'])->middleware('auth:petugas');
//ROUTE GURU
Route::get('guru', ['as'=>'guru','uses'=>'GuruController@index'])->middleware('auth:guru');

//ROUTE ADMIN
Route::get('admin', ['as'=>'admin','uses'=>'AdminController@index'])->middleware('auth:admin');
Route::get('/data_calon_siswa', ['as'=>'data_calon_siswa','uses'=>'AdminController@datacalonsiswa'])->middleware('auth:admin');
Route::post('/data_calon_siswa', ['as'=>'data_calon_siswa','uses'=>'AdminController@datacalonsiswa'])->middleware('auth:admin');
Route::get('/data_siswa_diterima', ['as'=>'data_siswa_diterima','uses'=>'AdminController@datasiswaditerima'])->middleware('auth:admin');
Route::post('/data_siswa_diterima', ['as'=>'data_siswa_diterima','uses'=>'AdminController@datasiswaditerima'])->middleware('auth:admin');
Route::get('/admin/data-karyawan', ['as'=>'admin-data-karyawan','uses'=>'AdminController@dataKaryawan'])->middleware('auth:admin');
Route::post('/admin/data-karyawan', ['as'=>'admin-data-karyawan','uses'=>'AdminController@dataKaryawan'])->middleware('auth:admin');
Route::get('/admin/data-guru', ['as'=>'admin-data-guru','uses'=>'AdminController@dataGuru'])->middleware('auth:admin');
Route::post('/admin/data-guru', ['as'=>'admin-data-guru','uses'=>'AdminController@dataGuru'])->middleware('auth:admin');
Route::get('/admin/pengaturan', ['as'=>'admin-pengaturan','uses'=>'AdminController@pengaturan'])->middleware('auth:admin');
Route::post('/admin/pengaturan', ['as'=>'admin-update-pengaturan','uses'=>'AdminController@pengaturan'])->middleware('auth:admin');
// Route::post('/admin/pengaturan', ['as'=>'admin-update-menu','uses'=>'AdminController@pengaturan'])->middleware('auth:admin');

Route::get('/admin/berita', ['as'=>'admin_berita','uses'=>'AdminController@adminberita'])->middleware('auth:admin');
Route::get('/logout/{guards}', 'LoginController@logout');

//ROUTE KOMEN
//LOGIN SOSMED
Route::get('auth/{provider}', 'LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'LoginController@handleProviderCallback');
Route::post('/berita/{tgl}/{judul}', ['as'=>'kirim-komentar','uses'=>'KomenController@kirim'])->middleware('auth:komen');


//
Route::get('commingson', ['as'=>'commingson','uses'=>'LoginController@commingson']);
