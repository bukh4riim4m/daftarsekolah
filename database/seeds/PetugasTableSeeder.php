<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class PetugasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('petugas')->insert(
          [
            'id'      => $i,
            'name'      => 'nama_petugas',
            'hp'      => '08111111111',
            'email'      => 'petugas@gmail.com',
            'password'       => bcrypt(123456),
            'admin_id'      => '1',
            'type'      => 'petugas',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      
    }
}
