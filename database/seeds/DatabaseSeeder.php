<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AgamasTableSeeder::class);
        $this->call(JenisKelaminTableSeeder::class);
        $this->call(PendapatansTableSeeder::class);
        $this->call(PendidikansTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(YatimPiyatuTableSeeder::class);

        $this->call(AdminsTableSeeder::class);
        $this->call(PetugasTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(BannersTableSeeder::class);
        $this->call(PengumumanTableSeeder::class);
    }
}
