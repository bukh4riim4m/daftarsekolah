<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
class JenisKelaminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_kelamins')->insert(
            [
              'jenis_kelamin'      => 'Laki - Laki',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('jenis_kelamins')->insert(
            [
              'jenis_kelamin'      => 'Perempuan',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
    }
}
