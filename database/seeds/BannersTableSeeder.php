<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('banners')->insert(
        [
          'id'=>1,
          'title'      => 'SMA N 1 SAPE',
          'logo'      => 'logo_sma_1_sape.png',
          'telpon'      => '021-9988776655',
          'email'      => 'info@gmail.com',
          'alamat'      => 'banner.jpg',
          'admin_id'      => '1',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);
    }
}
