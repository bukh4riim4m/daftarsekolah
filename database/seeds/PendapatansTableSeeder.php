<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
class PendapatansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pendapatans')->insert(
            [
              'pendapatan'      => 'Kurang Dari Rp 1,000,000',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('pendapatans')->insert(
            [
              'pendapatan'      => 'Rp 1,000,000 S/d Rp 3,000,000',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('pendapatans')->insert(
            [
              'pendapatan'      => 'Rp 3,000,000 S/d Rp 5,000,000',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('pendapatans')->insert(
            [
              'pendapatan'      => 'Rp 5,000,000 S/d Rp 10,000,000',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('pendapatans')->insert(
            [
              'pendapatan'      => 'Lebih dari Rp 10,000,000',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
    }
}
