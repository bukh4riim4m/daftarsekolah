<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
class AgamasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agamas')->insert(
            [
              'agama'      => 'Islam',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('agamas')->insert(
            [
              'agama'      => 'Kristen',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('agamas')->insert(
            [
              'agama'      => 'Protestan',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('agamas')->insert(
            [
              'agama'      => 'Hindu',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('agamas')->insert(
            [
              'agama'      => 'Budha',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
    }
}
