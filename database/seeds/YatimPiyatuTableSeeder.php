<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;

class YatimPiyatuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('yatim_piyatus')->insert(
            [
              'name'      => 'Masih Lengkap',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('yatim_piyatus')->insert(
            [
              'name'      => 'Anak Yatim',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('yatim_piyatus')->insert(
            [
              'name'      => 'Anak Piyatu',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('yatim_piyatus')->insert(
            [
              'name'      => 'Yatim Piyatu',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
    }
}
