<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
        'name'=>'Imam bukhari', 
        'email'=>'bukhariimam44@gmail.com',
        'password' => bcrypt(123456),
        'hp'=>'082312543008',
        'nama_panggilan'=>'Imam',
        'jenis_kelamin_id'=>'1',
        'tempat_lahir'=>'Jakarta',
        'tgl_lahir'=>'1988-02-12',
        'alamat_lengkap'=>'Jl. Harapan',
        'sekolah_asal'=>'SMP N 3 BIMA',
        'alamat_sekolah_asal'=>'Jl. Lapangan putih sangia',
        'jumlah_saudara_kandung'=>1,
        'jumlah_saudara_tiri'=>0,
        'jumlah_saudara_angkat'=>0,
        'yatim_piyatu_id'=>'1',
        'bahasa_sehari_hari'=>'Indonesia',
        'bangsa_negara'=>'Indonesia',
        'kelainan_jasmani'=>'Tidak',
        'penyakit'=>'Tidak',
        'tinggi_badan'=>169,
        'berat_badan'=>58,
        'prestasi'=>'Membuat website',
        'nama_ayah'=>'Imran',
        'tempat_lahir_ayah'=>'Bima',
        'tgl_lahir_ayah'=>'1966-01-01',
        'agama_ayah_id'=>1,
        'pendiddikan_ayah_id'=>3,
        'pekerjaan_ayah'=>'Smart',
        'warga_negara_ayah'=>'Indonesia',
        'nomor_telpon_ayah'=>'0812345667',
        'pedapatan_ayah'=>3,
        'nama_ibu'=>'Mujnah',
        'tempat_lahir_ibu'=>'Bima',
        'tgl_lahir_ibu'=>'1977-02-02',
        'agama_ibu_id'=>1,
        'pendiddikan_ibu_id'=>3,
        'pekerjaan_ibu'=>'IRT',
        'warga_negara_ibu'=>'Indonesia',
        'nomor_telpon_ibu'=>'085333221144',
        'status_id'=>1,
        'pedapatan_ibu'=>3,
        'type'=>'user',
        'petugas_id'=>1
            ]);
    }
}
