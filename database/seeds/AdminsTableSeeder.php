<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Admin;
use Carbon\Carbon;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

          DB::table('admins')->insert(
          [
            'id'      => $i,
            'name'      => 'nama-admin',
            'hp'      => '08121212121212',
            'email'      => 'admin@gmail.com',
            'password'       => bcrypt(123456),
            'admin_id'      => 1,
            'type'      => 'admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
      
    }
}
