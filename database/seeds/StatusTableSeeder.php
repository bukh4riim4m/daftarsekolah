<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(
            [
              'status'      => 'Calon Siswa',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('statuses')->insert(
            [
              'status'      => 'Dalam Proses',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
          DB::table('statuses')->insert(
            [
              'status'      => 'Siswa Baru',
              'aktif'      => 1,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now()
          ]);
    }
}
