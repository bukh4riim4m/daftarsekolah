<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlurPendaftaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alur_pendaftarans', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tgl_mulai');
            $table->date('tgl_selesai');
            $table->string('judul');
            $table->text('keterangan');
            $table->enum('is_active',['yes','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alur_pendaftarans');
    }
}
