<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomentarBeritasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_beritas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('beritas_id');
            $table->foreign('beritas_id')->references('id')->on('beritas');
            $table->string('nama');
            $table->string('hp');
            $table->text('komentar');
            $table->enum('is_active',['yes','no']);
            $table->unsignedBigInteger('userkomen_id');
            $table->foreign('userkomen_id')->references('id')->on('user_komens');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_beritas');
    }
}
