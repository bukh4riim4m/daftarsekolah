<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('name',150);
          $table->string('hp',15);
          $table->string('email',150)->unique();
          $table->timestamp('email_verified_at')->nullable();
          $table->string('password');
          $table->unsignedBigInteger('admin_id');
          $table->foreign('admin_id')->references('id')->on('admins');
          $table->string('type',20)->default('admin');
          $table->rememberToken();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
