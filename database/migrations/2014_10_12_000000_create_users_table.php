<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',150);
            $table->string('email',150)->unique()->nullable();
            $table->string('hp',15);
            $table->string('foto');
            $table->string('nomor_pendaftaran',20);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('nama_panggilan',100);
            $table->unsignedBigInteger('jenis_kelamin_id');
            $table->foreign('jenis_kelamin_id')->references('id')->on('jenis_kelamins');
            $table->string('tempat_lahir',100);
            $table->date('tgl_lahir')->nullable();
            $table->text('alamat_lengkap');
            $table->string('sekolah_asal',200);
            $table->text('alamat_sekolah_asal');
            $table->integer('jumlah_saudara_kandung');
            $table->integer('jumlah_saudara_tiri');
            $table->integer('jumlah_saudara_angkat');
            $table->unsignedBigInteger('yatim_piyatu_id');
            $table->foreign('yatim_piyatu_id')->references('id')->on('yatim_piyatus');
            $table->string('bahasa_sehari_hari',100);
            $table->string('bangsa_negara',100);
            $table->string('kelainan_jasmani',100);
            $table->string('penyakit',100);
            $table->integer('tinggi_badan');
            $table->integer('berat_badan');
            $table->string('prestasi',200);
            //ayah
            $table->string('nama_ayah',100);
            $table->string('tempat_lahir_ayah',100);
            $table->date('tgl_lahir_ayah');
            $table->unsignedBigInteger('agama_ayah_id');
            $table->foreign('agama_ayah_id')->references('id')->on('agamas');
            $table->unsignedBigInteger('pendiddikan_ayah_id');
            $table->foreign('pendiddikan_ayah_id')->references('id')->on('pendidikans');
            $table->string('pekerjaan_ayah',200);
            $table->string('warga_negara_ayah',100);
            $table->string('nomor_telpon_ayah',100);
            $table->unsignedBigInteger('pedapatan_ayah');
            $table->foreign('pedapatan_ayah')->references('id')->on('pendapatans');
            //ibu
            $table->string('nama_ibu',100);
            $table->string('tempat_lahir_ibu',100);
            $table->date('tgl_lahir_ibu');
            $table->unsignedBigInteger('agama_ibu_id');
            $table->foreign('agama_ibu_id')->references('id')->on('agamas');
            $table->unsignedBigInteger('pendiddikan_ibu_id');
            $table->foreign('pendiddikan_ibu_id')->references('id')->on('pendidikans');
            $table->string('pekerjaan_ibu',200);
            $table->string('warga_negara_ibu',100);
            $table->string('nomor_telpon_ibu',100);
            $table->unsignedBigInteger('pedapatan_ibu');
            $table->foreign('pedapatan_ibu')->references('id')->on('pendapatans');
            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->unsignedBigInteger('petugas_id');
            $table->foreign('petugas_id')->references('id')->on('petugas');
            $table->enum('aktif',['yes','no']);
            $table->string('type',20)->default('user');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
